<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Dompdf\Dompdf;
use Dompdf\Options;

use App\Entity\TGeneral;
use App\Services\JwtAuth;
use App\Entity\TConsultas;
use App\Entity\TRespuestasConsulta;

class GenerarPDFController extends AbstractController
{

    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    // public function GenerarPDF(){

    //     // Configurar Dompdf
    //     $pdfOptions = new Options();
    //     $pdfOptions->set('defaultFont', 'Arial');
        
    //     // Crear instancia de Dompf con la configuracion
    //     $dompdf = new Dompdf($pdfOptions);
    //     $nombres = 'Luis Ayala';
        
    //     // Recuperar HTML
    //     $html = $this->renderView('generar_pdf/index.html.twig', [
    
    //         'nombres' => $nombres
            
    //     ]);

    //     // Cargar HTML en el Dompdf
    //     $dompdf->loadHtml($html);
        
    //     // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
    //     $dompdf->setPaper('A4', 'portrait');

    //     // Renderizar el HTML en PDF
    //     $dompdf->render();

    //     // $pdf = 'contrato'.$documento.'.pdf';
    //     $nombrePDF = '/contrato.pdf';

    //     // Generar PDF y forzar descarga del archivo
    //     //  $dompdf->stream($pdf, [
    //     //     "Attachment" => true
    //     // ]);

    //     $output = $dompdf->output();
        
    //     // Seleccionamos el directorio que se configura en el services.yaml

    //     $pdf_directory = $this->getParameter('pdf_directory');
    //     $publicDirectory = $pdf_directory;
    //     // e.g /var/www/project/public/mypdf.pdf
    //     $pdfFilepath =  $publicDirectory . $nombrePDF;

    //     // Se guarda el pdf en la ruta especificada
    //     file_put_contents($pdfFilepath, $output);

    //     $data = [
    //         'status' => 'success',
    //         'code' => '200',
    //         'message' => 'PDF generado con éxito'
    //     ];                          
                
    //     return $this->resjson($data);

    // }

    public function GenerarPDF(Request $request){

        /*
            En este método se genera un archivo con los correspondientes .
            ---------------------------------------------------------------
            CORPOSOFT
        */

        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();
        $files_pdf = $this->getParameter('files_pdf');

        $fecha = new \DateTime('now', new \DateTimeZone('America/Bogota'));
        $fechaEstadistica = $fecha->format('YmdHis');

        //Se establecen las configuraciones para la generación del PDF
        //============================================================
 
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
    
        $dompdf = new Dompdf($pdfOptions);

        $items = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];

        $template = $this->renderView('generar_pdf/index.html.twig', [
            'nombres' => $items   
        ]);

        $dompdf->loadHtml($template);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $nombrePDF = '/apoderados'.$fechaEstadistica.'.pdf';
        $output = $dompdf->output();
        
        $pdfFilepath =  $files_pdf.$nombrePDF;
        file_put_contents($pdfFilepath, $output);

        $data = [
            'status' => 'success',
            'code' => '200',
            'message' => 'PDF generado con éxito'
        ];                          
        
        return $this->resjson($data);
        
    }

}
