<?php

namespace App\Controller;

use Symfony\Component\Validator\Validation;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\TVotos;
use App\Entity\TRoles;
use App\Entity\TGeneral;
use App\Entity\TSesiones;
use App\Entity\TRegistro;
use App\Entity\TUsuarios;
use App\Services\JwtAuth;
use App\Entity\TApoderado;
use App\Services\SendEmail;
use App\Services\ServiceSMS;

class TUsuariosController extends AbstractController
{

    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }
    
    public function Login(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza el ingreso al sistena mediante un usuario y una contraseña
            que son validadas por el sistema.
            ------------------------------------------------------------------------------------
            CORPOSOFT
        */

        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();

        //Obtener parametros del json
        //===========================

        $json = $request->get('json', null);
        $params = json_decode($json);

        if($json != null){

            $usuario = !empty($params->usuario) ? $params->usuario : null;
            $password = !empty($params->password) ? $params->password : null;
            $gettoken = !empty($params->gettoken) ? $params->gettoken : null;

            if(!empty($usuario) && !empty($password)){

                $pwd = hash('sha256', $password);

                if($gettoken){

                    $signup = $jwt_auth->signup($usuario, $password, $gettoken);

                }else{

                    $signup = $jwt_auth->signup($usuario, $password);

                }

                return new JsonResponse($signup);

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor complete los campos',
                ];

            }

        } else {

            $data = [
                'status' => 'error',
                'code' => '400',
                'message' => 'Json vacío',
            ];

        }

        return $this->resjson($data);
    }

    public function Registrar(Request $request){

        /*
            En este método se realiza el registro de usuarios pendientes de verificación.
            -----------------------------------------------------------------------------
            CORPOSOFT
        */

        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();

        //Obtener parametros del json
        //===========================

        $json = $request->get('json', null);
        $params = json_decode($json); 

        $estadoInactivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
            'valor' => 'I'
        ]);

        if($json = !null){

            $rol = !empty($params->rol) ? $params->rol : null;
            $correo = !empty($params->correo) ? $params->correo : null;
            $nombres = !empty($params->nombres) ? $params->nombres : null;
            $apellidos = !empty($params->apellidos) ? $params->apellidos : null;
            $documento = !empty($params->documento) ? $params->documento : null;
            $tipoDocumento = !empty($params->tipoDocumento) ? $params->tipoDocumento : null;

            if(!empty($nombres) && !empty($apellidos) && !empty($rol) && !empty($documento) && !empty($tipoDocumento)){

                $nombresTrim = trim($nombres);
                $nombres = $nombresTrim;

                $apellidosTrim = trim($apellidos);
                $apellidos = $apellidosTrim;

                $rol_ = $doctrine->getRepository(TRoles::class)->findOneBy([
                    'valor' => $rol
                ]);

                $tipoDocumento_ = $doctrine->getRepository(TGeneral::class)->findOneBy([
                    'valor' => $tipoDocumento
                ]);

                $documentUnique = $doctrine->getRepository(TUsuarios::class)->findOneBy([
                    'documento' => $documento
                ]);

                if(empty($documentUnique)){

                    //Registro del usuario
                    //====================

                    $usuario = new TUsuarios();
                    $usuario->setRol($rol_);
                    $usuario->setCorreo($correo);
                    $usuario->setDocumento($documento);
                    $usuario->setEstado($estadoInactivo);
                    $usuario->setNombres(strtoupper($nombres));
                    $usuario->setTipoDocumento($tipoDocumento_);
                    $usuario->setApellidos(strtoupper($apellidos));

                    $em->persist($usuario);
                    $em->flush();
    
                    $data = [
                        'status' => 'success',
                        'code' => '200',
                        'message' => 'Usuario registrado con éxito',
                        'data' => $usuario
                    ];

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'Ya existe un usuario registrado con este número de documento'
                    ];
                
                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor complete todos los campos',
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '400',
                'message' => 'Json vacío',
            ];

        }

        return $this->resjson($data);

    }

    public function Listar(Request $request, PaginatorInterface $paginator, JwtAuth $jwt_auth, $opc){

         /*
           En este metodo se listan todos los registros almacenados en la tabla t_usuarios.
           --------------------------------------------------------------------------------
           CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
           
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtenemos parámetro
            //===================

            $idUsuario = $request->get('idUsuario');
            $pagination = $request->get('pagination');

            $estadoActivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
                'valor' => 'A'
            ]);

            $rolUsuario = $doctrine->getRepository(TRoles::class)->findOneBy([
                'valor' => 'U'
            ]);

            switch($opc){

                case 1:

                    //Se obtiene la Lista de todos los usuarios registrados en t_usuarios
                    //====================================================================

                    $sqlUsuarios = $em->createQueryBuilder()
                        ->select('u') 
                        ->from('App\Entity\TUsuarios','u')
                        ->where('u.estado = :estado AND u.rol = :rol')
                        ->setParameter('estado', $estadoActivo->getId())
                        ->setParameter('rol', $rolUsuario->getId())
                        ->orderBy('u.id', 'DESC')
                    ;
            
                    $result = $sqlUsuarios->getQuery()->getResult();

                    if(!empty($result)){

                        if(!empty($pagination)){

                            $page = $request->query->getInt('page', $pagination);
                            $items_per_page = 10;
                            $pagination = $paginator->paginate($result, $page, $items_per_page);
                            $total = $pagination->getTotalItemCount();

                            $usuarios = [
                                'total_item_count' => $total,
                                'page_actual' => $page,
                                'items_per_page' => $items_per_page,
                                'total_pages' => ceil($total / $items_per_page),
                                'usuarios' => $pagination
                            ];

                        }else{

                            $page = $request->query->getInt('page', 1);
                            $items_per_page = 10;
                            $pagination = $paginator->paginate($result, $page, $items_per_page);
                            $total = $pagination->getTotalItemCount();

                            $usuarios = [
                                'total_item_count' => $total,
                                'page_actual' => $page,
                                'items_per_page' => $items_per_page,
                                'total_pages' => ceil($total / $items_per_page),
                                'usuarios' => $pagination
                            ];

                        }

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de usuarios',
                            'data' => $usuarios
                        ];

                    }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontraron usuarios para listar',
                        'data' => 0
                    ];

                    } 
                    
                break;
                
                case 2:

                    //Se obtiene un usaurio mediante su id
                    //====================================

                    if(!empty($idUsuario)){

                        $usuario = $doctrine->getRepository(TUsuarios::class)->find($idUsuario);
        
                        if(!empty($usuario)){

                            $data = [
                                'status' => 'success',
                                'code' => '200',
                                'message' => 'Información del usuario',
                                'data' => $usuario
                            ];
    
                        }else{

                            $data = [
                                'status' => 'success',
                                'code' => '300',
                                'message' => 'No se encontró el usuario con en el id '.$idUsuario,
                                'data' => 0
                            ];

                        }

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'Por favor ingrese un id de usuario'
                        ];

                    }
                    
                break;
            }

        }else{

                $data = [
                    'status' => 'error',
                    'code' => '100',
                    'message' => 'Usuario no autenticado'
                ];

        }

        return $this->resjson($data);

    }

    public function ConfirmarUsuarios(Request $request, JwtAuth $jwt_auth){
    }

    public function validarToken(Request $request, JwtAuth $jwt_auth){

        /* 
            En este método se verifca la validez del token 
            ----------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        //Se valida si el token esta vigente
        //==================================

        if($authCheck){

            $data = [
                'status' => 'success',
                'code' => '200',
                'message' => 'Usuario autenticado.',
                'token' => true,
            ];

        }else{

            $data = [
                'status' => 'error',
                'code' => '400',
                'message' => 'Usuario no autenticado.',
                'token' => false,
            ];

        }

        return $this->resjson($data);
            
    }

    public function ActivarUsuarios(Request $request, JwtAuth $jwt_auth, SendEmail $email, \Swift_Mailer $mailer){

        /*
            En este método se realiza la activación de un usuario mediante su id.
            ---------------------------------------------------------------------
            CORPOSOFT
        */
        
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();

        //Se obtiene parámetro
        //====================

        $idUsuario = $request->get('idUsuario');

        if(!empty($idUsuario)){

            $usuario = $doctrine->getRepository(TUsuarios::class)->find($idUsuario);

            if(!empty($usuario)){

                //Se valida el estado del usuario
                //===============================

                if($usuario->getEstado()->getValor() == 'I'){

                    //Se genera un usuario y una contraseña que serán enviadas vía email
                    //==================================================================

                    $dataEmail = [$mailer, 'leandroayala883@gmail.com', 'leandroayala883@gmail.com'];

                    $result = $email->sendEmail($dataEmail);
                    return new JsonResponse($result);

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'Este usuario ya está activo',
                        'data' => 0
                    ];

                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'No se encontró el usuario con en el id '.$idUsuario,
                    'data' => $_SERVER['REMOTE_ADDR'].' - '.$_SERVER['HTTP_USER_AGENT']
                ];

            }


        }else{

            $data = [
                'status' => 'success',
                'code' => '300',
                'message' => 'Por favor ingrese un id de usuario'
            ];

        }

        return $this->resjson($data);

    }

    public function Actualizar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se actualiza la informacion de un usuario. Para realizar esta
            operación se busca el usuario mediante un id. 
            -----------------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Se obtiene parámetro
            //====================

            $idUsuario = $request->get('idUsuario');

            if(!empty($idUsuario)){

                $usuario = $doctrine->getRepository(TUsuarios::class)->find($idUsuario);

                if(!empty($usuario)){

                    //Obtener parametros del json
                    //===========================

                    $json = $request->get('json', null);
                    $params = json_decode($json);

                    if($json != null){

                        $rol = !empty($params->rol) ? $params->rol : null;
                        $estado = !empty($params->estado) ? $params->estado : null;
                        $user = !empty($params->user) ? $params->user : $usuario->getUsuario();
                        $correo = !empty($params->correo) ? $params->correo : $usuario->getCorreo();
                        $tipoDocumento = !empty($params->tipoDocumento) ? $params->tipoDocumento : null;
                        $nombres = !empty($params->nombres) ? $params->nombres : $usuario->getNombres();
                        $fechaExp = !empty($params->fechaExp) ? $params->fechaExp : $usuario->getFechaExp();
                        $apellidos = !empty($params->apellidos) ? $params->apellidos : $usuario->getApellidos();
                        $documento = !empty($params->documento) ? $params->documento : $usuario->getDocumento();
                        $numeroAcciones = !empty($params->numeroAcciones) ? $params->numeroAcciones : $usuario->getAcciones();

                        //Se asignan nuevo valores a las fkey
                        //===================================

                        if(!empty($rol)){

                            $rol_ = $doctrine->getRepository(TRoles::class)->findOneBy([
                                'valor' => $rol
                            ]);

                            $rol_ = $rol_;

                        }else{

                            $rol_ = $usuario->getRol();

                        }

                        if(!empty($estado)){

                            $estado_ = $doctrine->getRepository(TGeneral::class)->findOneBy([
                                'valor' => $estado
                            ]);

                            $estado_ = $estado_;

                        }else{

                            $estado_ = $usuario->getEstado();

                        }

                        if(!empty($tipoDocumento)){

                            $tipoDocumento_ = $doctrine->getRepository(TGeneral::class)->findOneBy([
                                'valor' => $tipoDocumento
                            ]);

                            $tipoDocumento_ = $tipoDocumento_;

                        }else{

                            $tipoDocumento_ = $usuario->getTipoDocumento();

                        }

                        //Se valida la existencia del documento y el usuario
                        //==================================================

                        $documentUnique = $doctrine->getRepository(TUsuarios::class)->findOneBy([
                            'documento' => $documento
                        ]);

                        if(empty($documentUnique)){

                            $estadoDocumento = 1;

                        }else{

                            if($usuario->getDocumento() == $documento){

                                $estadoDocumento = 1;

                            }else{

                                $estadoDocumento = 0;

                            }

                        }

                        //Se valida el estado del documento
                        //=================================

                        if($estadoDocumento > 0){

                            $userUnique = $doctrine->getRepository(TUsuarios::class)->findOneBy([
                                'usuario' => $user
                            ]);
    
                            if(empty($userUnique)){
    
                                $estadoUsuario = 1;
    
                            }else{
    
                                if($usuario->getUsuario() == $userUnique->getUsuario()){
    
                                    $estadoUsuario = 1;
    
                                }else{
                                    
                                    $estadoUsuario = 0;
    
                                }
    
                            }

                            //Se valida el estado del usuario
                            //===============================

                            if($estadoUsuario > 0){


                                //Actualización del usuario
                                //==========================

                                $usuario->setRol($rol_);
                                $usuario->setUsuario($user);
                                $usuario->setCorreo($correo);
                                $usuario->setEstado($estado_);
                                $usuario->setFechaExp($fechaExp);
                                $usuario->setDocumento($documento);
                                $usuario->setAcciones($numeroAcciones);
                                $usuario->setNombres(strtoupper($nombres));
                                $usuario->setTipoDocumento($tipoDocumento_);
                                $usuario->setApellidos(strtoupper($apellidos));

                                $em->persist($usuario);
                                $em->flush();
                
                                $data = [
                                    'status' => 'success',
                                    'code' => '200',
                                    'message' => 'Usuario actualizado con éxito',
                                    'data' => $usuario
                                ];
   
                            }else{

                                $data = [
                                    'status' => 'success',
                                    'code' => '300',
                                    'message' => 'Este usuario ya está en uso',
                                    'data' => 0
                                ];
                                
                            }

                        }else{

                            $data = [
                                'status' => 'success',
                                'code' => '300',
                                'message' => 'Ya existe un usuario registrado con este número de documento',
                                'data' => 0
                            ];

                        }

                    }else{

                        $data = [
                            'status' => 'error',
                            'code' => '400',
                            'message' => 'Json vacío',
                        ];
                        
                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontró el usuario con en el id '.$idUsuario,
                        'data' => 0
                    ];

                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese un id de usuario'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function RegistrarConToken(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza el registro de usuarios pendientes de verificación.
            -----------------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parametros del json
            //===========================

            $json = $request->get('json', null);
            $params = json_decode($json); 

            $estadoInactivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
                'valor' => 'I'
            ]);

            if($json = !null){

                $rol = !empty($params->rol) ? $params->rol : null;
                $correo = !empty($params->correo) ? $params->correo : null;
                $nombres = !empty($params->nombres) ? $params->nombres : null;
                $apellidos = !empty($params->apellidos) ? $params->apellidos : null;
                $documento = !empty($params->documento) ? $params->documento : null;
                $tipoDocumento = !empty($params->tipoDocumento) ? $params->tipoDocumento : null;

                if(!empty($nombres) && !empty($apellidos) && !empty($rol) && !empty($documento) && !empty($tipoDocumento)){

                    $nombresTrim = trim($nombres);
                    $nombres = $nombresTrim;

                    $apellidosTrim = trim($apellidos);
                    $apellidos = $apellidosTrim;

                    $rol_ = $doctrine->getRepository(TRoles::class)->findOneBy([
                        'valor' => $rol
                    ]);

                    $tipoDocumento_ = $doctrine->getRepository(TGeneral::class)->findOneBy([
                        'valor' => $tipoDocumento
                    ]);

                    $documentUnique = $doctrine->getRepository(TUsuarios::class)->findOneBy([
                        'documento' => $documento
                    ]);

                    if(empty($documentUnique)){

                        //Registro del usuario
                        //====================

                        $usuario = new TUsuarios();
                        $usuario->setRol($rol_);
                        $usuario->setCorreo($correo);
                        $usuario->setDocumento($documento);
                        $usuario->setEstado($estadoInactivo);
                        $usuario->setNombres(strtoupper($nombres));
                        $usuario->setTipoDocumento($tipoDocumento_);
                        $usuario->setApellidos(strtoupper($apellidos));

                        $em->persist($usuario);
                        $em->flush();
        
                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Usuario registrado con éxito',
                            'data' => $usuario
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'Ya existe un usuario registrado con este número de documento'
                        ];
                    
                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'Por favor complete todos los campos',
                    ];

                }

            }else{

                $data = [
                    'status' => 'error',
                    'code' => '400',
                    'message' => 'Json vacío',
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ]; 
            
        }

        return $this->resjson($data);

    }

    public function CargaMasivaUsuarios(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza la carga masiva de usuarios.
            ------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parametros del json
            //===========================
    
            $json = $request->get('json', null);
            $usuarios = json_decode($json);

            $estadoInactivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
                'valor' => 'I'
            ]);

            $rolUsuario = $doctrine->getRepository(TRoles::class)->findOneBy([
                'valor' => 'U'
            ]);

            $valorPorDefecto = $doctrine->getRepository(TGeneral::class)->findOneBy([
                'valor' => 'SAG'
            ]);

            if($json != null){

                //Se itera la lista de usuarios
                //=============================

                if(!empty($usuarios->users)){

                    $countUsuariosRegister = 0;

                    foreach($usuarios->users as $u){

                        $nombres = $u->nombres;
                        $fechaExp = $u->fechaExp;
                        $apellidos = $u->apellidos;
                        $documento = $u->documento;
                        $tipoDocumento = $u->tipoDocumento;

                        if(!empty($tipoDocumento)){

                            $tipoDocumento_ = $doctrine->getRepository(TGeneral::class)->findOneBy([
                                'valor' => $tipoDocumento
                            ]);

                        }else{

                            $tipoDocumento_ = $valorPorDefecto;

                        }

                        //Se verifica la existencia del usuario
                        //=====================================

                        if(!empty($documento) && $documento != '000'){

                            $documentUnique = $doctrine->getRepository(TRegistro::class)->findOneBy([
                                'documento' => $documento
                            ]);

                            if(empty($documentUnique)){

                                $estadoDocumento = 1;

                            }else{

                                $estadoDocumento = 0;

                            }

                        }else{

                            $documento = '000';
                            $estadoDocumento = 1;

                        }

                        //Se valida el estado del documento
                        //=================================

                        if($estadoDocumento > 0){

                            //Registro del usuario
                            //====================

                            $usuario = new TRegistro();
                            $usuario->setRol($rolUsuario);
                            $usuario->setDocumento($documento);
                            $usuario->setEstado($estadoInactivo);
                            $usuario->setNombres(strtoupper($nombres));
                            $usuario->setTipoDocumento($tipoDocumento_);
                            $usuario->setFechaExp(strtoupper($fechaExp));
                            $usuario->setApellidos(strtoupper($apellidos));

                            $em->persist($usuario);
                            $em->flush();
                            $countUsuariosRegister ++;

                        }   

                    }

                    $data = [
                        'status' => 'success',
                        'code' => '200',
                        'message' => 'Se registraron '.$countUsuariosRegister.'/'.count($usuarios->users).' usuarios al sistema'
                    ];

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontraron usuarios para registrar'
                    ];

                }   

            }else{

                $data = [
                    'status' => 'error',
                    'code' => '400',
                    'message' => 'Json vacío',
                ];
                
            }
            
        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function ValidarUsuarios(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza la validación de un usuario mediante su número de documento.
            --------------------------------------------------------------------------------------
            CORPOSOFT
        */

        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();

        //Se obtiene parámetro
        //====================

        $documento = $request->get('documento');

        if(!empty($documento)){

            $usuario = $doctrine->getRepository(TRegistro::class)->findOneBy([
                'documento' => $documento
            ]);

            if(!empty($usuario)){

                if($usuario->getEstado()->getValor() == 'I'){

                    $estadoRegistro = [
                        'usuario' => $usuario,
                        'estadoRegistro' => true
                    ];

                }else{

                    $estadoRegistro = [
                        'estadoRegistro' => true
                    ];

                }

                $data = [
                    'status' => 'success',
                    'code' => '200',
                    'message' => 'Usuario inactivo',
                    'data' => $estadoRegistro
                ];             

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'No se encontró el usuario con el número de documento '.$documento,
                    'data' => 0
                ];

            }

        }else{

            $data = [
                'status' => 'success',
                'code' => '300',
                'message' => 'Por favor ingrese un número de documento'
            ];

        }

        return $this->resjson($data);

    }

    public function RegistrarUsuarios(Request $request, JwtAuth $jwt_auth, SendEmail $email, ServiceSMS $sms, \Swift_Mailer $mailer){

        /*
            En este método se realiza el registro de usuarios y se valida su estado en 
            la tabla t_registro.
            --------------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parametros del json
            //===========================

            $json = $request->get('json', null);
            $params = json_decode($json); 
            $image = $this->getParameter('files_media');
            $template = $this->renderView('email/index.html.twig');

            $estadoInactivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
                'valor' => 'I'
            ]);

            $estadoActivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
                'valor' => 'A'
            ]);

            //Generar contraseña aleatoria y crifrada
            //=======================================
    
            $caracteres = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $passwordRandom = substr(str_shuffle($caracteres), 0, 6);
            $pwd = hash('sha256', $passwordRandom);

            if($json != null){

                $rol = !empty($params->rol) ? $params->rol : null;
                $correo = !empty($params->correo) ? $params->correo : null;
                $celular = !empty($params->celular) ? $params->celular : null;
                $nombres = !empty($params->nombres) ? $params->nombres : null;
                $fechaExp = !empty($params->fechaExp) ? $params->fechaExp : null;
                $apellidos = !empty($params->apellidos) ? $params->apellidos : null;
                $documento = !empty($params->documento) ? $params->documento : null;
                $tipoDocumento = !empty($params->tipoDocumento) ? $params->tipoDocumento : null;
                $numeroAcciones = !empty($params->numeroAcciones) ? $params->numeroAcciones : null;

                if(!empty($nombres) && !empty($apellidos) && !empty($rol) && !empty($documento) && !empty($tipoDocumento) && 
                   !empty($fechaExp) && !empty($numeroAcciones) && !empty($celular)){

                    $nombresTrim = trim($nombres);
                    $nombres = $nombresTrim;

                    $apellidosTrim = trim($apellidos);
                    $apellidos = $apellidosTrim;

                    $rol_ = $doctrine->getRepository(TRoles::class)->findOneBy([
                        'valor' => $rol
                    ]);

                    $tipoDocumento_ = $doctrine->getRepository(TGeneral::class)->findOneBy([
                        'valor' => $tipoDocumento
                    ]);

                    /*
                        PASSWORD
                        ----------------------------------------------------------------------------------
                        Generamos el usuario de la persona con base en su primer nombre y la inicial de su
                        primer apellido. 
                    */
    
                    $numeroUser = 1;
                    $formatUser = explode(' ', $nombres);
                    $user = strtolower($formatUser[0] . $apellidos[0]);
                    $usuario = $user . $numeroUser;
                    $nombresCompletos = strtoupper($nombres).' '.strtoupper($apellidos);

                    $documentUnique = $doctrine->getRepository(TUsuarios::class)->findOneBy([
                        'documento' => $documento
                    ]);

                    if(empty($documentUnique)){

                        do{

                            $userUnique = $doctrine->getRepository(TUsuarios::class)->findOneBy([
                                'usuario' => $usuario
                            ]);
    
                            if(!empty($userUnique)){

                                $numeroUser ++;
                                $usuario = $user . $numeroUser;

                            }else{

                                $usuario = $usuario;

                            }

                        }while(!empty($userUnique));

                        //Registro del usuario
                        //====================

                        $user = new TUsuarios();
                        $user->setRol($rol_);
                        $user->setCorreo($correo);
                        $user->setUsuario($usuario);
                        $user->setContraseina($pwd);
                        $user->setFechaExp($fechaExp);
                        $user->setDocumento($documento);
                        $user->setEstado($estadoActivo);
                        $user->setAcciones($numeroAcciones);
                        $user->setNombres(strtoupper($nombres));
                        $user->setTipoDocumento($tipoDocumento_);
                        $user->setApellidos(strtoupper($apellidos));

                        $em->persist($user);
                        $em->flush();
                        
                        //Se actualiza el estado de t_registro
                        //====================================

                        $registro = $doctrine->getRepository(TRegistro::class)->findOneBy([
                            'documento' => $documento
                        ]);

                        if(!empty($registro)){  

                            $registro->setEstado($estadoActivo);
                            $em->persist($registro);
                            $em->flush();

                            //$dataEmail = [$mailer, 'corsoft@corsoft.com', $correo, $image, $usuario, $passwordRandom, $nombresCompletos, $user, $template];

                            //$result = $email->sendEmail($dataEmail);
                            //return new JsonResponse($result);

                            $sms->setLogin('leandroayala883@gmail.com');
                            $sms->setPassword('9b7a45sb');
                            $sms->setDebug(true);

                            $destination = '573183809554';
                            $response = $sms->sendSMS('57'.trim($celular), 'Hola '.$nombresCompletos." estos son tus datos de acceso a CORPOSOFT.\nUsuario: ".$usuario."\nPassword: ".$passwordRandom);

                        }else{

                            //$dataEmail = [$mailer, 'corsoft@corsoft.com', $correo, $image, $usuario, $passwordRandom, $nombresCompletos, $user, $template];

                            //$result = $email->sendEmail($dataEmail);
                            //return new JsonResponse($result);

                            $sms->setLogin('leandroayala883@gmail.com');
                            $sms->setPassword('9b7a45sb');
                            $sms->setDebug(true);

                            $destination = '573183809554';
                            $response = $sms->sendSMS('57'.trim($celular), 'Hola '.$nombresCompletos." estos son tus datos de acceso a CORPOSOFT.\nUsuario: ".$usuario."\nPassword: ".$passwordRandom);

                        } 

                        if(!$response){

                            $data = [
                                'status' => 'success',
                                'code' => '200',
                                'message' => 'Usuario registrado con éxito. Ocurrió un error con el envío del mensaje de texto',
                                'data' => $user
                            ];

                        }else{

                            $data = [
                                'status' => 'success',
                                'code' => '200',
                                'message' => 'Usuario registrado con éxito. Hemos enviado tus credenciales de acceso al móvil',
                                'data' => $user
                            ];

                        }
        
                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'Ya existe un usuario registrado con este número de documento'
                        ];
                    
                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'Por favor complete todos los campos',
                    ];

                }

            }else{

                $data = [
                    'status' => 'error',
                    'code' => '400',
                    'message' => 'Json vacío',
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ]; 
            
        }

        return $this->resjson($data);

    }

    public function ListarUsuarios(Request $request, JwtAuth $jwt_auth){

        /*
           En este método se listan todos los usuarios registradas en t_usuarios.
           ----------------------------------------------------------------------
           CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
           
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            $estadoActivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
                'valor' => 'A'
            ]);

            $rolUsuario = $doctrine->getRepository(TRoles::class)->findOneBy([
                'valor' => 'U'
            ]);

            $sqlUsuarios = $em->createQueryBuilder()
                ->select('u.id, u.nombres, u.apellidos') 
                ->from('App\Entity\TUsuarios','u')
                ->where('u.estado = :estado AND u.rol = :rol')
                ->setParameter('estado', $estadoActivo->getId())
                ->setParameter('rol', $rolUsuario->getId())
                ->orderBy('u.id', 'DESC')
            ;

            $result = $sqlUsuarios->getQuery()->getResult();

            if(!empty($result)){

                $data = [
                    'status' => 'success',
                    'code' => '200',
                    'message' => 'Lista de usuarios',
                    'data' => $result
                ];

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'No se encontraron usuarios para listar',
                    'data' => 0
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);
        
    }

    public function RegistrarUsuariosSinToken(Request $request, JwtAuth $jwt_auth, SendEmail $email, ServiceSMS $sms, \Swift_Mailer $mailer){

        /*
            En este método se realiza el registro de usuarios y se valida su estado en 
            la tabla t_registro.
            --------------------------------------------------------------------------
            CORPOSOFT
        */

        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();

        //Obtener parametros del json
        //===========================

        $json = $request->get('json', null);
        $params = json_decode($json); 
        $image = $this->getParameter('files_media');
        $template = $this->renderView('email/index.html.twig');

        $estadoInactivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
            'valor' => 'I'
        ]);

        $estadoActivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
            'valor' => 'A'
        ]);

        //Generar contraseña aleatoria y crifrada
        //=======================================

        $caracteres = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $passwordRandom = substr(str_shuffle($caracteres), 0, 6);
        $pwd = hash('sha256', $passwordRandom);

        if($json != null){

            $rol = !empty($params->rol) ? $params->rol : null;
            $correo = !empty($params->correo) ? $params->correo : null;
            $celular = !empty($params->celular) ? $params->celular : null;
            $nombres = !empty($params->nombres) ? $params->nombres : null;
            $fechaExp = !empty($params->fechaExp) ? $params->fechaExp : null;
            $apellidos = !empty($params->apellidos) ? $params->apellidos : null;
            $documento = !empty($params->documento) ? $params->documento : null;
            $tipoDocumento = !empty($params->tipoDocumento) ? $params->tipoDocumento : null;
            $numeroAcciones = !empty($params->numeroAcciones) ? $params->numeroAcciones : null;

            if(!empty($nombres) && !empty($apellidos) && !empty($rol) && !empty($documento) && !empty($tipoDocumento) && 
               !empty($fechaExp) && !empty($numeroAcciones) && !empty($celular)){

                $nombresTrim = trim($nombres);
                $nombres = $nombresTrim;

                $apellidosTrim = trim($apellidos);
                $apellidos = $apellidosTrim;

                $rol_ = $doctrine->getRepository(TRoles::class)->findOneBy([
                    'valor' => $rol
                ]);

                $tipoDocumento_ = $doctrine->getRepository(TGeneral::class)->findOneBy([
                    'valor' => $tipoDocumento
                ]);

                /*
                    PASSWORD
                    ----------------------------------------------------------------------------------
                    Generamos el usuario de la persona con base en su primer nombre y la inicial de su
                    primer apellido. 
                */

                $numeroUser = 1;
                $formatUser = explode(' ', $nombres);
                $user = strtolower($formatUser[0] . $apellidos[0]);
                $usuario = $user . $numeroUser;
                $nombresCompletos = strtoupper($nombres).' '.strtoupper($apellidos);

                $documentUnique = $doctrine->getRepository(TUsuarios::class)->findOneBy([
                    'documento' => $documento
                ]);

                if(empty($documentUnique)){

                    do{

                        $userUnique = $doctrine->getRepository(TUsuarios::class)->findOneBy([
                            'usuario' => $usuario
                        ]);

                        if(!empty($userUnique)){

                            $numeroUser ++;
                            $usuario = $user . $numeroUser;

                        }else{

                            $usuario = $usuario;

                        }

                    }while(!empty($userUnique));

                    //Registro del usuario
                    //====================

                    $user = new TUsuarios();
                    $user->setRol($rol_);
                    $user->setCorreo($correo);
                    $user->setUsuario($usuario);
                    $user->setContraseina($pwd);
                    $user->setFechaExp($fechaExp);
                    $user->setDocumento($documento);
                    $user->setEstado($estadoActivo);
                    $user->setAcciones($numeroAcciones);
                    $user->setNombres(strtoupper($nombres));
                    $user->setTipoDocumento($tipoDocumento_);
                    $user->setApellidos(strtoupper($apellidos));

                    $em->persist($user);
                    $em->flush();
                    
                    //Se actualiza el estado de t_registro
                    //====================================

                    $registro = $doctrine->getRepository(TRegistro::class)->findOneBy([
                        'documento' => $documento
                    ]);

                    if(!empty($registro)){  

                        $registro->setEstado($estadoActivo);
                        $em->persist($registro);
                        $em->flush();

                        $sms->setLogin('leandroayala883@gmail.com');
                        $sms->setPassword('9b7a45sb');
                        $sms->setDebug(true);

                        $destination = '573183809554';
                        $response = $sms->sendSMS('57'.trim($celular), 'Hola '.$nombresCompletos." estos son tus datos de acceso a CORPOSOFT.\nUsuario: ".$usuario."\nPassword: ".$passwordRandom);


                        // $dataEmail = [$mailer, 'corsoft@corsoft.com', $correo, $image, $usuario, $passwordRandom, $nombresCompletos, $user, $template];

                        // $result = $email->sendEmail($dataEmail);
                        // return new JsonResponse($result);              

                    }else{

                        $sms->setLogin('leandroayala883@gmail.com');
                        $sms->setPassword('9b7a45sb');
                        $sms->setDebug(true);

                        $destination = '573183809554';
                        $response = $sms->sendSMS('57'.trim($celular), 'Hola '.$nombresCompletos." estos son tus datos de acceso a CORPOSOFT.\nUsuario: ".$usuario."\nPassword: ".$passwordRandom);
                        

                        // $dataEmail = [$mailer, 'corsoft@corsoft.com', $correo, $image, $usuario, $passwordRandom, $nombresCompletos, $user, $template];

                        // $result = $email->sendEmail($dataEmail);
                        // return new JsonResponse($result);
                    } 

                    if(!$response){

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Usuario registrado con éxito. Ocurrió un error con el envío del mensaje de texto',
                            'data' => $user
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Usuario registrado con éxito. Hemos enviado tus credenciales de acceso al móvil',
                            'data' => $user
                        ];

                    }
    
                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'Ya existe un usuario registrado con este número de documento'
                    ];
                
                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor complete todos los campos',
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '400',
                'message' => 'Json vacío',
            ];

        }
      
        return $this->resjson($data);

    }

    public function CargarCSV(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza la carga de cvs para el registro de usuarios.
            -----------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parámetro
            //=================

            $file = $request->files->get('file0');

            $fsObject = new Filesystem();
            $allFiles = $request->files;

            $estadoInactivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
                'valor' => 'I'
            ]);

            $rolUsuario = $doctrine->getRepository(TRoles::class)->findOneBy([
                'valor' => 'U'
            ]);

            $valorPorDefecto = $doctrine->getRepository(TGeneral::class)->findOneBy([
                'valor' => 'SAG'
            ]);

            //Carga del archivo
            //=================

            if(!empty($file)){

                $folder_root = $this->getParameter('folder_root');
                $files_uploads = $this->getParameter('files_uploads');
                $fsObject->mkdir($folder_root.'/uploads', 0755);
                $files_uploads = $folder_root.'/uploads';

                $filename = 'csv_'.$identity->documento.'.csv';
                $file->move(
                    $files_uploads,
                    $filename
                );

                //Se recorreo el archivo para obtener los usuarios
                //================================================

                $archivo = fopen($folder_root.'/uploads/'.'csv_'.$identity->documento.'.csv', 'r');
                $listUsersRegister = [];
                $indexUsersRegister = 0;
                $listUsersFound = [];
                $indexUsersFound = 0;
                $listUsers = [];
                $index = 0;

                while(($datos = fgetcsv($archivo, ",")) == true){
                    
                    $num = count($datos);
                    for($columna = 1; $columna < $num; $columna++){
                        
                        $dataUser = [
                            'nombres' => $datos[0],
                            'apellidos' => $datos[1],
                            'tipoDocumento' => $datos[2],
                            'documento' => $datos[3],
                            'fechaExp' => $datos[4]
                        ];
                        
                    }

                    $listUsers[$index] = $dataUser;
                    $index ++;

                }

                fclose($archivo);

                unset($listUsers[0]);

                //Se vaida la lista de usuarios y se realiza la inserción de los usuarios
                //=======================================================================

                if(!empty($listUsers)){

                    $countUsuariosRegister = 0;

                    foreach($listUsers as $lu){

                        if(!empty($lu['tipoDocumento'])){

                            $tipoDocumento_ = $doctrine->getRepository(TGeneral::class)->findOneBy([
                                'valor' => $lu['tipoDocumento']
                            ]);
        
                        }else{
        
                            $tipoDocumento_ = $valorPorDefecto;
        
                        }
        
                        //Se verifica la existencia del usuario
                        //=====================================
        
                        if(!empty($lu['documento']) && $lu['documento'] != '000'){
        
                            $documentUnique = $doctrine->getRepository(TRegistro::class)->findOneBy([
                                'documento' => $lu['documento']
                            ]);
        
                            if(empty($documentUnique)){
        
                                $estadoDocumento = 1;
        
                            }else{
        
                                $estadoDocumento = 0;
                                $listUsersFound[$indexUsersFound] = $documentUnique;
                                $indexUsersFound ++;
        
                            }
        
                        }else{
        
                            $documento = '000';
                            $estadoDocumento = 1;

                        }
        
                        //Se valida el estado del documento
                        //=================================
        
                        if($estadoDocumento > 0){
        
                            //Registro del usuario
                            //====================
        
                            $usuario = new TRegistro();
                            $usuario->setRol($rolUsuario);
                            $usuario->setEstado($estadoInactivo);
                            $usuario->setFechaExp($lu['fechaExp']);
                            $usuario->setDocumento($lu['documento']);
                            $usuario->setTipoDocumento($tipoDocumento_);
                            $usuario->setNombres(strtoupper($lu['nombres']));
                            $usuario->setApellidos(strtoupper($lu['apellidos']));
        
                            $em->persist($usuario);
                            $em->flush();

                            $listUsersRegister[$indexUsersRegister] = $usuario;
                            $countUsuariosRegister ++;
                            $indexUsersRegister ++;
        
                        }   

                    }

                    $dataUserFoundRegister = [
                        'usuariosRegistrados' => $listUsersRegister,
                        'usuariosNoRegistrados' => $listUsersFound
                    ];

                    $data = [
                        'status' => 'success',
                        'code' => '200',
                        'message' => 'Se registraron '.$countUsuariosRegister.'/'.count($listUsers).' usuarios al sistema',
                        'data' => $dataUserFoundRegister
                    ];

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontraron usuarios para registrar'
                    ];

                }
        
            }else{

                $data = [
                    'status' => 'error',
                    'code' => '400',
                    'message' => 'Por favor seleccione un archivo'
                ];

 
            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);
        
    }

    public function ValidarApoderado(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza la validación de un apoderado.
            --------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();
    
            //Se obtiene parámetro
            //====================
    
            $idUsuario = $request->get('idUsuario');
    
            if(!empty($idUsuario)){
    
                $usuario = $doctrine->getRepository(TUsuarios::class)->find($idUsuario);
    
                if(!empty($usuario)){
    
                    //Se verifica si el usuario es un apoderado
                    //=========================================
    
                    $apoderado_ = $doctrine->getRepository(TApoderado::class)->findBy([
                        'apoderado' => $usuario->getId()
                    ]);
    
                    if(!empty($apoderado_) && $usuario->getEstado()->getValor() == 'A'){

                        //Se verifica si el apoderado es votante
                        //======================================

                        $votos = $doctrine->getRepository(TVotos::class)->findBy([
                            'usuario' => $identity->sub
                        ]);

                        if(!empty($votos)){

                            $votante = true;

                        }else{

                            $votante = false;

                        }
    
                        $apoderado = true;
                        
                    }else{
    
                        $apoderado = false;
                        $votante = false;
    
                    }

                    $dataApoderado = [
                        'apoderado' => $apoderado,
                        'votante' => $votante
                    ];
    
                    $data = [
                        'status' => 'success',
                        'code' => '200',
                        'message' => 'Usuario apoderado',
                        'data' => $dataApoderado
                    ];             
    
                }else{
    
                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontró el usuario con el id '.$idUsuario,
                        'data' => 0
                    ];
    
                }
    
            }else{
    
                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese un id de usuario'
                ];
    
            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function CerrarSesion(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se cierra la sesión actual de un dispositivo.
            ------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            $fecha = new \DateTime('now', new \DateTimeZone('America/Bogota'));
            $fechaCierreSesion = $fecha->format('Y-m-d H:i:s');

            //Se obtienen las sesiones del usuario
            //====================================

            $sesiones = $doctrine->getRepository(TSesiones::class)->findBy([
                'usuario' => $identity->sub
            ]);

            //Se verifica la existencia de una sesión abierta
            //===============================================

            $sessionOn = [];

            foreach($sesiones as $s){

                if($s->getFechaCierreSesion() == '0000-00-00'){

                    $sessionOn = $s;

                }

            }

            if(!empty($sessionOn)){

                //Se cierra la sesión y se actualiza la fecha de cierre de sesión
                //===============================================================

                $sessionOn->setFechaCierreSesion($fechaCierreSesion);
                $em->persist($sessionOn);
                $em->flush();

                $data = [
                    'status' => 'success',
                    'code' => '200',
                    'message' => 'Se ha cerrado la sesión'
                ];

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'No se encontraron sesiones abiertas para este usuario',
                    'data' => 0
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }
        
        return $this->resjson($data);

    }

    public function CerrarTodasSesiones(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se cierran todas las sesiones abiertas de un usuario.
            --------------------------------------------------------------------
            CORPOSOFT
        */
        
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();

        //Obtenemos parámetro
        //===================

        $idUsuario = $request->get('idUsuario');

        $fecha = new \DateTime('now', new \DateTimeZone('America/Bogota'));
        $fechaCierreSesion = $fecha->format('Y-m-d H:i:s');

        if(!empty($idUsuario)){

            $usuario = $doctrine->getRepository(TUsuarios::class)->find($idUsuario);

            if(!empty($usuario)){
                
                //Se obtiene la ip y el navegador donde se inicio sesión desde el dispositivo
                //===========================================================================

                $browser = ["IE","OPERA","MOZILLA","NETSCAPE","FIREFOX","SAFARI","CHROME"];
                
                $info['browser'] = "OTHER";
                $info['version'] = "OTHER";
            
                foreach($browser as $parent){

                    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
                    $f = $s + strlen($parent);
                    $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 15);
                    $version = preg_replace('/[^0-9,.]/','',$version);

                    if($s){

                        $info['browser'] = $parent;
                        $info['version'] = $version;

                    }
                }

                if(!empty($idUsuario)){

                    $usuario = $doctrine->getRepository(Tusuarios::class)->find($idUsuario);

                    if(!empty($usuario)){
            
                        //Se obtienen las sesiones del usuario
                        //====================================

                        $sesiones = $doctrine->getRepository(TSesiones::class)->findBy([
                            'usuario' => $usuario->getId()
                        ]);

                        //Se verifica la existencia de una sesión abierta
                        //===============================================

                        $sessionOn = [];

                        foreach($sesiones as $s){

                            if($s->getFechaCierreSesion() == '0000-00-00'){

                                $sessionOn = $s;

                            }

                        }

                        if(!empty($sessionOn)){

                            $observacionCierreSesion = '<strong>Sesión cerrada desde la ip: </strong>'.$_SERVER['REMOTE_ADDR'].'<br>
                                                        <strong>Navegador: </strong>'.$info['browser'].' - '.$info['version'];

                            //Se cierra la sesión y se actualiza la fecha de cierre de sesión
                            //===============================================================

                            $sessionOn->setFechaCierreSesion($fechaCierreSesion);
                            $sessionOn->setObservacion($observacionCierreSesion);
                            $em->persist($sessionOn);
                            $em->flush();

                            $data = [
                                'status' => 'success',
                                'code' => '200',
                                'message' => 'Se han cerrado las sesiones abiertas'
                            ];

                        }else{

                            $data = [
                                'status' => 'success',
                                'code' => '300',
                                'message' => 'No se encontraron sesiones abiertas para este usuario',
                                'data' => 0
                            ];

                        }

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontró el usuario con el id '.$idUsuario,
                            'data' => 0
                        ];

                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'Por favor ingrese un id de usuario'
                    ];

                }


            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'No se encontró el usuario con id '.$idUsuario,
                    'data' => 0
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '400',
                'message' => 'Por favor ingrese un id de usuario'
            ];

        }
 
        return $this->resjson($data);

    }

}
