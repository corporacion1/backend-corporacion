<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Services\JwtAuth;
use App\Entity\TUsuarios;
use App\Entity\TPlanchas;
use App\Entity\TUsuariosPlancha;

class TUsuariosPlanchaController extends AbstractController
{
    
    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function Registrar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza el registro de usuarios a una plancha.
            ----------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Se obtiene parámetro
            //====================

            $idPlancha = $request->get('idPlancha');

            //Obtener parametros del json
            //===========================
    
            $json = $request->get('json', null);
            $usuariosPlancha = json_decode($json);

            $plancha = $doctrine->getRepository(TPlanchas::class)->find($idPlancha);

            if(!empty($plancha)){

                if($json != null){

                    if($plancha->getTipo() == 'C'){

                        //Se itera la lista de usuarios
                        //=============================
        
                        if(!empty($usuariosPlancha)){
        
                            //Se valida que los usuarios a registrar estén completos
                            //======================================================
        
                            $index = [0,1];
                            $countRegister = 1;
                            $countUsers = count($usuariosPlancha);
        
                            if($countUsers == 7){
        
                                foreach($usuariosPlancha as $u){
        
                                    foreach($index as $i){
        
                                        //Se asiga el tipo de candidato a paritr de la posición del arreglo
                                        //=================================================================
        
                                        if($i == 0){
        
                                            $tipo = 'Principal';
                                            $usuario = $doctrine->getRepository(TUsuarios::class)->find($u[0]);
                                            $principal = $usuario;
        
                                        }else{
        
                                            $tipo = 'Suplente';
                                            $usuario = $doctrine->getRepository(TUsuarios::class)->find($u[1]);
                                            $principal = $doctrine->getRepository(TUsuarios::class)->find($u[0]);
        
                                        }
                                        
                                        $userPlancha = new TUsuariosPlancha();
                                        $userPlancha->setTipo($tipo);
                                        $userPlancha->setPlancha($plancha);
                                        $userPlancha->setUsuario($usuario);
                                        $userPlancha->setPrincipal($principal);
                
                                        $em->persist($userPlancha);
                                        $em->flush();
        
                                    }
        
                                    $countRegister ++;
            
                                }
            
                                $data = [
                                    'status' => 'success',
                                    'code' => '200',
                                    'message' => 'Usuarios registrados con éxito'
                                ];
        
                            }else{
        
                                $data = [
                                    'status' => 'success',
                                    'code' => '300',
                                    'message' => 'Verifique que los usuarios a registrar sean 7'
                                ];
        
                            }
        
                        }else{
        
                            $data = [
                                'status' => 'success',
                                'code' => '300',
                                'message' => 'No se encontraron usuarios para registrar'
                            ];
        
                        }   

                    }else{

                        //Se itera la lista de usuarios
                        //=============================
        
                        if(!empty($usuariosPlancha)){
        
                            //Se valida que los usuarios a registrar estén completos
                            //======================================================
        
                            $index = [0,1];
                            $countRegister = 1;
                            $countUsers = count($usuariosPlancha);
        
                            if($countUsers == 1){
        
                                foreach($usuariosPlancha as $u){
        
                                    foreach($index as $i){
        
                                        //Se asiga el tipo de candidato a paritr de la posición del arreglo
                                        //=================================================================
        
                                        if($i == 0){
        
                                            $tipo = 'Principal';
                                            $usuario = $doctrine->getRepository(TUsuarios::class)->find($u[0]);
                                            $principal = $usuario;
        
                                        }else{
        
                                            $tipo = 'Suplente';
                                            $usuario = $doctrine->getRepository(TUsuarios::class)->find($u[1]);
                                            $principal = $doctrine->getRepository(TUsuarios::class)->find($u[0]);
        
                                        }
                                        
                                        $userPlancha = new TUsuariosPlancha();
                                        $userPlancha->setTipo($tipo);
                                        $userPlancha->setPlancha($plancha);
                                        $userPlancha->setUsuario($usuario);
                                        $userPlancha->setPrincipal($principal);
                
                                        $em->persist($userPlancha);
                                        $em->flush();
        
                                    }
        
                                    $countRegister ++;
            
                                }
            
                                $data = [
                                    'status' => 'success',
                                    'code' => '200',
                                    'message' => 'Usuarios registrados con éxito'
                                ];
        
                            }else{
        
                                $data = [
                                    'status' => 'success',
                                    'code' => '300',
                                    'message' => 'Solo es posible registrar un revisor fiscal'
                                ];
        
                            }
        
                        }else{
        
                            $data = [
                                'status' => 'success',
                                'code' => '300',
                                'message' => 'No se encontraron usuarios para registrar'
                            ];
        
                        }   
    
                    }
    
                }else{
    
                    $data = [
                        'status' => 'error',
                        'code' => '400',
                        'message' => 'Json vacío',
                    ];
                    
                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'No se encontraró la plancha con el id '.$idPlancha,
                    'data' => 0
                ];

            }
            
        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }
}
