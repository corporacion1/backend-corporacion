<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\TGeneral;
use App\Services\JwtAuth;
use App\Entity\TConsultas;
use App\Entity\TRespuestasConsulta;


class TRespuestasConsultaController extends AbstractController
{
    
    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function Registrar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza el registro de consultas (Preguntas).
            ---------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parametros del json
            //===========================

            $json = $request->get('json', null);
            $opciones = json_decode($json);

            //Se obtiene parámetro
            //====================

            $idConsulta = $request->get('idConsulta');

            if(!empty($idConsulta)){

                $consulta = $doctrine->getRepository(TConsultas::class)->find($idConsulta);

                if(!empty($consulta)){

                    if($json = !null){

                        if(!empty($opciones)){
    
                            //Se registran las respectivas respuestas para cada pregunta
                            //==========================================================

                            $respuestasRegister = 0;
    
                            foreach($opciones as $op){

                                $opcion = $doctrine->getRepository(TGeneral::class)->findOneBy([
                                    'valor' => $op
                                ]);
    
                                $respuesta = new TRespuestasConsulta();
                                $respuesta->setOpcion($opcion);
                                $respuesta->setConsulta($consulta);
                                $em->persist($respuesta);
                                $em->flush();

                                $respuestasRegister ++;
    
                            }
    
                            $data = [
                                'status' => 'success',
                                'code' => '200',
                                'message' => 'Se han registrado '.$respuestasRegister.' respuestas'
                            ];
        
                        }else{
        
                            $data = [
                                'status' => 'success',
                                'code' => '300',
                                'message' => 'No se encontraron opciones',
                                'data' => 0
                            ];
        
                        }
        
                    }else{
        
                        $data = [
                            'status' => 'error',
                            'code' => '400',
                            'message' => 'Json vacío'
                        ];
        
                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontró la consulta con el id '.$idConsulta,
                        'data' => 0
                    ];

                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese un id de consulta'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function Listar(Request $request, PaginatorInterface $paginator, JwtAuth $jwt_auth, $opc){

        /*
           En este método se listan todas las respuestas registradas en t_respuestas_consultas.
           También se lista la información de una respuesta mediante un id especifico.
           ------------------------------------------------------------------------------------
           CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
           
           $identity = $jwt_auth->checkToken($token, true);
           $doctrine = $this->getDoctrine();
           $em = $doctrine->getManager();

           //Obtenemos parámetro
           //===================

           $idRespuesta = $request->get('idRespuesta');
           $pagination = $request->get('pagination');

           $estadoActivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
                'valor' => 'A'
            ]);

            switch($opc){

                case 1:

                    //Se obtiene la lista de respuestas
                    //=================================

                    $sqlRespuestas = $em->createQueryBuilder()
                        ->select('r') 
                        ->from('App\Entity\TRespuestasConsulta','r')
                        ->orderBy('r.id', 'DESC')
                    ;

                    $result = $sqlRespuestas->getQuery()->getResult();

                    if(!empty($result)){

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de respuestas',
                            'data' => $result
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron respuestas para listar',
                            'data' => 0
                        ];

                    }
  
                break;

                case 2:

                    //Se lista la informacion de una respuesta mediante un id en específico
                    //=====================================================================

                    if(!empty($idRespuesta)){

                        $respuesta = $doctrine->getRepository(TRespuestasConsulta::class)->find($idRespuesta);

                        if(!empty($respuesta)){
                            
                            $data = [
                                'status' => 'success',
                                'code' => '200',
                                'message' => 'Información de la respuesta',
                                'data' => $respuesta
                            ];

                        }else{

                            $data = [
                                'status' => 'success',
                                'code' => '300',
                                'message' => 'No se encontró la respuesta con el id '.$idRespuesta,
                                'data' => 0
                            ];

                        }

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'Por favor ingrese un id de respuesta'
                        ];

                    }
                
                break;

                case 3:

                    //Se obtiene la lista de respuestas con paginator
                    //===============================================

                    $sqlRespuestas = $em->createQueryBuilder()
                        ->select('r') 
                        ->from('App\Entity\TRespuestasConsulta','r')
                        ->orderBy('r.id', 'DESC')
                    ;

                    $result = $sqlRespuestas->getQuery()->getResult();

                    if(!empty($result)){

                        if(!empty($pagination)){

                            $page = $request->query->getInt('page', $pagination);
                            $items_per_page = 10;
                            $pagination = $paginator->paginate($result, $page, $items_per_page);
                            $total = $pagination->getTotalItemCount();

                            $respuestas = [
                                'total_item_count' => $total,
                                'page_actual' => $page,
                                'items_per_page' => $items_per_page,
                                'total_pages' => ceil($total / $items_per_page),
                                'respuestas' => $pagination
                            ];

                        }else{

                            $page = $request->query->getInt('page', 1);
                            $items_per_page = 10;
                            $pagination = $paginator->paginate($result, $page, $items_per_page);
                            $total = $pagination->getTotalItemCount();

                            $respuestas = [
                                'total_item_count' => $total,
                                'page_actual' => $page,
                                'items_per_page' => $items_per_page,
                                'total_pages' => ceil($total / $items_per_page),
                                'respuestas' => $pagination
                            ];

                        }

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de respuestas',
                            'data' => $respuestas
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron respuestas para listar',
                            'data' => 0
                        ];

                    }

                break;

                case 4:

                    //Se obtiene la lista de respuestas activas
                    //=========================================

                    $sqlRespuestas = $em->createQueryBuilder()
                        ->select('r') 
                        ->from('App\Entity\TRespuestasConsulta','r')
                        ->where('r.estado = :estado')
                        -setParameter('estado', $estadoActivo->getId())
                        ->orderBy('r.id', 'DESC')
                    ;

                    $result = $sqlRespuestas->getQuery()->getResult();

                    if(!empty($result)){

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de respuestas',
                            'data' => $result
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron respuestas para listar',
                            'data' => 0
                        ];

                    }
  
                break;
               
            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

}
