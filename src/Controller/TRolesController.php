<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;                                
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\TRoles;

class TRolesController extends AbstractController
{

    private function resjson($data){

        //Metodo para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function Listar(){

        /*
            En este método se listan todos los roles de la tabla t_roles sin paginator.
            ---------------------------------------------------------------------------
            CORPOSOFT
        */

        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();

        $sqlRoles = $em->createQueryBuilder()
            ->select('r.nombre, r.valor') 
            ->from('App\Entity\TRoles','r')
            ->orderBy('r.nombre', 'ASC')
        ;

        $result = $sqlRoles->getQuery()->getResult();

        if(!empty($result)){
     
            $data = [
                'status' => 'success',
                'code' => '200',
                'message' => 'Lista de roles',
                'data' => $result
            ];

        }else{

            $data = [
                'status' => 'success',
                'code' => '300',
                'message' => 'No existen roles para listar',
                'data' => 0
            ];

        }

        return $this->resjson($data);

    }

}
