<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\TVotos;
use App\Entity\TGeneral;
use App\Entity\TPlanchas;
use App\Services\JwtAuth;
use App\Entity\TUsuarios;
use App\Entity\TApoderado;

class TVotosController extends AbstractController
{

    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function Registrar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se resliza el registro de votaciones.
            ----------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Se obtiene parámetro
            //====================

            $idPlancha = $request->get('idPlancha');

            $fecha = new \DateTime('now', new \DateTimeZone('America/Bogota'));
            $fechaVotacion = $fecha->format('Y-m-d H:i:s');

            $apoderado = $doctrine->getRepository(TUsuarios::class)->find($identity->sub);

            //Se obtiene el sistema operativo, el navegador y la versión
            //==========================================================

            $os = ["WIN","MAC","LINUX"];
            $browser = ["IE","OPERA","MOZILLA","NETSCAPE","FIREFOX","SAFARI","CHROME"];
        
            $info['os'] = "OTHER";
            $info['browser'] = "OTHER";
            $info['version'] = "OTHER";
        
            foreach($browser as $parent){

                $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
                $f = $s + strlen($parent);
                $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 15);
                $version = preg_replace('/[^0-9,.]/','',$version);

                if($s){

                    $info['browser'] = $parent;
                    $info['version'] = $version;

                }
            }
        
            foreach($os as $val){

                if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']),$val)!==false){

                    $info['os'] = $val;

                }

            }

            if(!empty($idPlancha)){

                $plancha = $doctrine->getRepository(TPlanchas::class)->find($idPlancha);

                if(!empty($plancha) && $plancha->getEstado()->getValor() == 'A'){

                    //Se verifica si el apoderado ya tiene un voto registrado
                    //=======================================================

                    $votoApoderado = $doctrine->getRepository(TVotos::class)->findOneBy([
                        'usuario' => $identity->sub
                    ]);

                    if(empty($votoApoderado)){

                        $voto = new TVotos();
                        $voto->setPlancha($plancha);
                        $voto->setUsuario($apoderado);
                        $voto->setSO($info['os']);
                        $voto->setFechaVoto($fechaVotacion);
                        $voto->setIpComputo($_SERVER['REMOTE_ADDR']);
                        $voto->setNavegador($info['browser'].' - '.$info['version']);

                        $em->persist($voto);
                        $em->flush();

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Voto registrado con éxito',
                            'data' => $voto
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'Este apoderado ya tiene registrado un voto'
                        ];

                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontró la plancha con el id '.$idPlancha,
                        'data' => 0
                    ];

                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese un id de plancha'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function AccionesPlancha(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se contabiliza el total de acciones por plancha.
            ---------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            $estadoActivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
                'valor' => 'A'
            ]);

            //Se obtienen todas las planchas activas
            //======================================

            $planchas = $doctrine->getRepository(TPlanchas::class)->findBy([
                'estado' => $estadoActivo->getId()
            ]);

            if(!empty($planchas)){

                //Se obtienen las acciones de cada plancha
                //========================================

                $index = 0;
                $totalAcciones = 0;
                $listPlanchas = [];

                foreach($planchas as $p){

                    $votos = $doctrine->getRepository(TVotos::class)->findBy([
                        'plancha' => $p->getId()
                    ]);

                    if(!empty($votos)){

                        //Se obtienen los apoderados y las respectivas acciones
                        //=====================================================

                        foreach($votos as $v){

                            $acciones = $doctrine->getRepository(TApoderado::class)->findBy([
                                'apoderado' => $v->getUsuario()->getId()
                            ]);

                            //Total de acciones por plancha
                            //=============================

                            foreach($acciones as $a){

                                $totalAcciones += $a->getAccionista()->getAcciones();

                            }

                        }

                    }else{

                        $totalAcciones = 0;

                    }

                    $dataPlancha = [
                        'id' => $p->getId(),
                        'plancha' => $p->getNombre(),
                        'totalAcciones' => $totalAcciones
                    ];

                    $listPlanchas[$index] = $dataPlancha;
                    $totalAcciones = 0;
                    $index ++;

                }

                $data = [
                    'status' => 'success',
                    'code' => '200',
                    'message' => 'Total acciones por plancha',
                    'data' => $listPlanchas
                ];

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'No se encontraron planchas activas para listar',
                    'data' => 0
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function VotacionesPlancha(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se contabiliza el total de votaciones por plancha.
            -----------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            $estadoActivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
                'valor' => 'A'
            ]);

            //Se obtienen todas las planchas activas
            //======================================

            $planchas = $doctrine->getRepository(TPlanchas::class)->findBy([
                'estado' => $estadoActivo->getId()
            ]);

            if(!empty($planchas)){

                //Se obtienen las acciones de cada plancha
                //========================================

                $index = 0;
                $totalVotos = 0;
                $listPlanchas = [];

                foreach($planchas as $p){

                    $votos = $doctrine->getRepository(TVotos::class)->findBy([
                        'plancha' => $p->getId()
                    ]);

                    if(!empty($votos)){

                        //Se obtienen el total de votos por plancha
                        //=====================================================

                        foreach($votos as $v){

                            $totalVotos ++;
    
                        }

                    }else{

                        $totalVotos = 0;

                    }

                    $dataPlancha = [
                        'id' => $p->getId(),
                        'plancha' => $p->getNombre(),
                        'totalVotos' => $totalVotos
                    ];

                    $listPlanchas[$index] = $dataPlancha;
                    $totalVotos = 0;
                    $index ++;

                }

                $data = [
                    'status' => 'success',
                    'code' => '200',
                    'message' => 'Total votos por plancha',
                    'data' => $listPlanchas
                ];

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'No se encontraron planchas activas para listar',
                    'data' => 0
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

}
