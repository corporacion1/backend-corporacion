<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\TVotos;
use App\Entity\TUsuarios;
use App\Services\JwtAuth;
use App\Entity\TApoderado;

class TApoderadoController extends AbstractController
{

    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function Registrar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se resliza el registro de apoderados.
            ----------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Se obtiene parámetro
            //====================

            $idApoderado = $request->get('idApoderado');
            $idAccionista = $request->get('idAccionista');

            if(!empty($idApoderado) && !empty($idAccionista)){

                //Se verifica la existencia del usuario apoderado
                //===============================================

                $apoderado_ = $doctrine->getRepository(TUsuarios::class)->find($idApoderado);

                if(!empty($apoderado_)){

                    //Se verifica la existencia del usuario accionista
                    //================================================

                    $accionista = $doctrine->getRepository(TUsuarios::class)->find($idAccionista);

                    if(!empty($accionista)){

                        $apoderado = new TApoderado();
                        $apoderado->setApoderado($apoderado_);
                        $apoderado->setAccionista($accionista);
                        $em->persist($apoderado);
                        $em->flush();

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Apoderado registrado con éxito',
                            'data' => $apoderado
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontró el usuario con el id '. $idAccionista,
                            'data' => 0
                        ];

                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontró el usuario con el id '. $idApoderado,
                        'data' => 0
                    ];

                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor complete los campos requeridos'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function Listar(Request $request, PaginatorInterface $paginator, JwtAuth $jwt_auth, $opc){

        /*
           En este método se listan todas los apoderados registradas en t_apoderados.También se lista 
           la información de un apoderado mediante un id especifico.
           ------------------------------------------------------------------------------------------
           CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
           
           $identity = $jwt_auth->checkToken($token, true);
           $doctrine = $this->getDoctrine();
           $em = $doctrine->getManager();

           //Obtenemos parámetro
           //===================

           $pagination = $request->get('pagination');
           $idApoderado = $request->get('idApoderado');

            switch($opc){

                case 1:

                    //Se obtiene la lista de apoderados
                    //=================================

                    $sqlApoderados = $em->createQueryBuilder()
                        ->select('a') 
                        ->from('App\Entity\TApoderado','a')
                        ->orderBy('a.id', 'DESC')
                    ;

                    $result = $sqlApoderados->getQuery()->getResult();

                    if(!empty($result)){

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de apoderados',
                            'data' => $result
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron apoderados para listar',
                            'data' => 0
                        ];

                    }
  
                break;

                case 2:

                    //Se lista la informacion de un apoderado mediante un id en específico
                    //====================================================================

                    if(!empty($idApoderado)){

                        $apoderado = $doctrine->getRepository(TApoderado::class)->find($idApoderado);

                        if(!empty($apoderado)){
                            
                            $data = [
                                'status' => 'success',
                                'code' => '200',
                                'message' => 'Información del apoderado',
                                'data' => $apoderado
                            ];

                        }else{

                            $data = [
                                'status' => 'success',
                                'code' => '300',
                                'message' => 'No se encontró el apoderado con el id '.$idApoderado,
                                'data' => 0
                            ];

                        }

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'Por favor ingrese un id de apoderado'
                        ];

                    }
                
                break;

                case 3:

                    //Se obtiene la lista de apoderados
                    //=================================

                    $sqlApoderados = $em->createQueryBuilder()
                        ->select('a') 
                        ->from('App\Entity\TApoderado','a')
                        ->orderBy('a.id', 'DESC')
                    ;

                    $result = $sqlApoderados->getQuery()->getResult();

                    if(!empty($result)){ 

                        if(!empty($pagination)){

                            $page = $request->query->getInt('page', $pagination);
                            $items_per_page = 10;
                            $pagination = $paginator->paginate($result, $page, $items_per_page);
                            $total = $pagination->getTotalItemCount();

                            $apoderados = [
                                'total_item_count' => $total,
                                'page_actual' => $page,
                                'items_per_page' => $items_per_page,
                                'total_pages' => ceil($total / $items_per_page),
                                'apoderados' => $pagination
                            ];

                        }else{

                            $page = $request->query->getInt('page', 1);
                            $items_per_page = 10;
                            $pagination = $paginator->paginate($result, $page, $items_per_page);
                            $total = $pagination->getTotalItemCount();

                            $apoderados = [
                                'total_item_count' => $total,
                                'page_actual' => $page,
                                'items_per_page' => $items_per_page,
                                'total_pages' => ceil($total / $items_per_page),
                                'apoderados' => $pagination
                            ];

                        }

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de apoderados',
                            'data' => $apoderados
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron apoderados para listar',
                            'data' => 0
                        ];

                    }

                break;
               
            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function ApoderadosVotantes(Request $request, PaginatorInterface $paginator, JwtAuth $jwt_auth, $opc){

        /*
            En este método se obtiene el total de apoderados votantes y pendientes de votación.
            -----------------------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parámetro
            //=================

            $pagination = $request->get('pagination');

            switch($opc){

                case 1:

                    //Lista de apoderados votantes
                    //============================

                    $votos = $doctrine->getRepository(TVotos::class)->findAll();

                    $index = 0;
                    $countApoderados = 0;
                    $listApoderados = [];

                    if(!empty($votos)){

                        //Se obtienen los apoderados y las respectivas acciones
                        //=====================================================     

                        foreach($votos as $v){

                            $listApoderados[$index] = $v->getUsuario();
                            $countApoderados ++;
                            $index ++;

                        }

                    }else{

                        $countApoderados = 0;
                        $listApoderados = 0;

                    }

                    //Lista ordenada de apoderados con paginator
                    //==========================================

                    sort($listApoderados,0);

                    if(!empty($pagination)){

                        $page = $request->query->getInt('page', $pagination);
                        $items_per_page = 10;
                        $pagination = $paginator->paginate($listApoderados, $page, $items_per_page);
                        $total = $pagination->getTotalItemCount();

                        $apoderados = [
                            'total_item_count' => $total,
                            'page_actual' => $page,
                            'items_per_page' => $items_per_page,
                            'total_pages' => ceil($total / $items_per_page),
                            'apoderados' => $pagination
                        ];

                    }else{

                        $page = $request->query->getInt('page', 1);
                        $items_per_page = 10;
                        $pagination = $paginator->paginate($listApoderados, $page, $items_per_page);
                        $total = $pagination->getTotalItemCount();

                        $apoderados = [
                            'total_item_count' => $total,
                            'page_actual' => $page,
                            'items_per_page' => $items_per_page,
                            'total_pages' => ceil($total / $items_per_page),
                            'apoderados' => $pagination
                        ];

                    }

                    $dataApoderados = [
                        'apoderados' => $apoderados,
                        'totalApoderados' => $countApoderados
                    ];

                    $data = [
                        'status' => 'success',
                        'code' => '200',
                        'message' => 'Apoderados votantes',
                        'data' => $dataApoderados
                    ];
                    
                break;

                case 2:

                    //Lista de apoderados no votantes
                    //===============================

                    $apoderados = $doctrine->getRepository(TApoderado::class)->findAll();

                    $index = 0;
                    $countApoderados = 0;
                    $listApoderados = [];
                    $indexApoderadoUnique = 0;
                    $listApoderadosUnique = [];

                    if(!empty($apoderados)){

                        foreach($apoderados as $a){

                            $listApoderadosUnique[$indexApoderadoUnique] = $a->getApoderado();
                            $indexApoderadoUnique ++;

                        }

                        //Lista unica de apoderados
                        //=========================

                        $apoderadosUnique = array_unique($listApoderadosUnique, 0);

                        //Se verifica si cada apoderado es votante
                        //========================================

                        foreach($apoderadosUnique as $ap){

                            $votos = $doctrine->getRepository(TVotos::class)->findBy([
                                'usuario' => $ap->getId()
                            ]);
                            
                            if(empty($votos)){

                                $listApoderados[$index] = $ap;
                                $countApoderados ++;
                                $index ++;

                            }

                        }

                    }else{

                        $countApoderados = 0;
                        $listApoderados = 0;

                    }

                    //Lista de apoderados con paginator
                    //=================================

                    if(!empty($pagination)){

                        $page = $request->query->getInt('page', $pagination);
                        $items_per_page = 10;
                        $pagination = $paginator->paginate($listApoderados, $page, $items_per_page);
                        $total = $pagination->getTotalItemCount();

                        $apoderados = [
                            'total_item_count' => $total,
                            'page_actual' => $page,
                            'items_per_page' => $items_per_page,
                            'total_pages' => ceil($total / $items_per_page),
                            'apoderados' => $pagination
                        ];

                    }else{

                        $page = $request->query->getInt('page', 1);
                        $items_per_page = 10;
                        $pagination = $paginator->paginate($listApoderados, $page, $items_per_page);
                        $total = $pagination->getTotalItemCount();

                        $apoderados = [
                            'total_item_count' => $total,
                            'page_actual' => $page,
                            'items_per_page' => $items_per_page,
                            'total_pages' => ceil($total / $items_per_page),
                            'apoderados' => $pagination
                        ];

                    }

                    $dataApoderados = [
                        'apoderados' => $apoderados,
                        'totalApoderados' => $countApoderados
                    ];

                    $data = [
                        'status' => 'success',
                        'code' => '200',
                        'message' => 'Apoderados no votantes',
                        'data' => $dataApoderados
                    ];
 
                break;
     
            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

}
