<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\TGeneral;
use App\Entity\TPlanchas;
use App\Services\JwtAuth;
use App\Entity\TUsuariosPlancha;

class TPlanchasController extends AbstractController
{

    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function Registrar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se resliza el registro de planchas.
            --------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Se obtiene parámetro
            //====================

            $tipo = $request->get('tipo');
            $nombre = $request->get('nombre');

            $estadoActivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
                'valor' => 'A'
            ]);

            if(!empty($nombre)){

                $plancha = new TPlanchas();
                $plancha->setTipo($tipo);
                $plancha->setNombre($nombre);
                $plancha->setEstado($estadoActivo);
                
                $em->persist($plancha);
                $em->flush();

                $data = [
                    'status' => 'success',
                    'code' => '200',
                    'message' => 'Plancha registrada con éxito',
                    'data' => $plancha
                ];

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese un nombre'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function Listar(Request $request, PaginatorInterface $paginator, JwtAuth $jwt_auth, $opc){

        /*
           En este método se listan todas las planchas registradas en t_planchas.También se lista 
           la información de una plancha mediante un id especifico.
           ---------------------------------------------------------------------------------------
           CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
           
           $identity = $jwt_auth->checkToken($token, true);
           $doctrine = $this->getDoctrine();
           $em = $doctrine->getManager();

           //Obtenemos parámetro
           //===================

           $idPlancha = $request->get('idPlancha');
           $pagination = $request->get('pagination');

            $estadoActivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
                'valor' => 'A'
            ]);

            switch($opc){

                case 1:

                    //Se obtiene la lista de planchas
                    //===============================

                    $sqlPlanchas = $em->createQueryBuilder()
                        ->select('p') 
                        ->from('App\Entity\TPlanchas','p')
                        ->orderBy('p.nombre', 'ASC')
                    ;

                    $result = $sqlPlanchas->getQuery()->getResult();

                    if(!empty($result)){

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de planchas',
                            'data' => $result
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron planchas para listar',
                            'data' => 0
                        ];

                    }
  
                break;

                case 2:

                    //Se lista la informacion de una plancha mediante un id en específico
                    //===================================================================

                    if(!empty($idPlancha)){

                        $plancha = $doctrine->getRepository(TPlanchas::class)->find($idPlancha);

                        if(!empty($plancha)){
                            
                            $data = [
                                'status' => 'success',
                                'code' => '200',
                                'message' => 'Información de la plancha',
                                'data' => $plancha
                            ];

                        }else{

                            $data = [
                                'status' => 'success',
                                'code' => '300',
                                'message' => 'No se encontró la plancha con el id '.$idPlancha,
                                'data' => 0
                            ];

                        }

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'Por favor ingrese un id de plancha'
                        ];

                    }
                
                break;

                case 3:

                    //Se obtiene la lista de planchas
                    //===============================

                    $sqlPlanchas = $em->createQueryBuilder()
                        ->select('p') 
                        ->from('App\Entity\TPlanchas','p')
                        ->orderBy('p.nombre', 'ASC')
                    ;

                    $result = $sqlPlanchas->getQuery()->getResult();

                    if(!empty($result)){

                        $index = 0;
                        $listPlanchas = [];

                        //Verificamos si la plancha tiene usuarios asignados
                        //==================================================

                        foreach($result as $r){

                            $usuariosPlancha = $doctrine->getRepository(TUsuariosPlancha::class)->findBy([
                                'plancha' => $r->getId()
                            ]);

                            if(!empty($usuariosPlancha)){

                                $estadoPlancha = true;

                            }else{

                                $estadoPlancha = false;

                            }

                            $dataPlancha = [
                                'plancha' => $r,
                                'ocupada' => $estadoPlancha
                            ];

                            $listPlanchas[$index] = $dataPlancha;
                            $index ++;

                        }

                        if(!empty($pagination)){

                            $page = $request->query->getInt('page', $pagination);
                            $items_per_page = 10;
                            $pagination = $paginator->paginate($listPlanchas, $page, $items_per_page);
                            $total = $pagination->getTotalItemCount();

                            $planchas = [
                                'total_item_count' => $total,
                                'page_actual' => $page,
                                'items_per_page' => $items_per_page,
                                'total_pages' => ceil($total / $items_per_page),
                                'planchas' => $pagination
                            ];

                        }else{

                            $page = $request->query->getInt('page', 1);
                            $items_per_page = 10;
                            $pagination = $paginator->paginate($listPlanchas, $page, $items_per_page);
                            $total = $pagination->getTotalItemCount();

                            $planchas = [
                                'total_item_count' => $total,
                                'page_actual' => $page,
                                'items_per_page' => $items_per_page,
                                'total_pages' => ceil($total / $items_per_page),
                                'planchas' => $pagination
                            ];

                        }

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de planchas',
                            'data' => $planchas
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron planchas para listar',
                            'data' => 0
                        ];

                    }

                break;

                case 4:

                    //Se obtiene la lista de planchas activas
                    //=======================================

                    $sqlPlanchas = $em->createQueryBuilder()
                        ->select('p') 
                        ->from('App\Entity\TPlanchas','p')
                        ->where('p.estado = :estado')
                        ->setParameter('estado', $estadoActivo->getId())
                        ->orderBy('p.nombre', 'ASC')
                    ;

                    $result = $sqlPlanchas->getQuery()->getResult();

                    if(!empty($result)){

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de planchas activas',
                            'data' => $result
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron planchas activas para listar',
                            'data' => 0
                        ];

                    }
  
                break;

                case 5:

                    //Se obtiene la lista de planchas tipo C activas
                    //==============================================

                    $sqlPlanchas = $em->createQueryBuilder()
                        ->select('p') 
                        ->from('App\Entity\TPlanchas','p')
                        ->where('p.estado = :estado AND p.tipo = :tipo')
                        ->setParameter('estado', $estadoActivo->getId())
                        ->setParameter('tipo', 'C')
                        ->orderBy('p.nombre', 'ASC')
                    ;

                    $result = $sqlPlanchas->getQuery()->getResult();

                    if(!empty($result)){

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de planchas tipo C',
                            'data' => $result
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron planchas tipo C para listar',
                            'data' => 0
                        ];

                    }
  
                break;

                case 6:

                    //Se obtiene la lista de planchas tipo C activas
                    //==============================================

                    $sqlPlanchas = $em->createQueryBuilder()
                        ->select('p') 
                        ->from('App\Entity\TPlanchas','p')
                        ->where('p.estado = :estado AND p.tipo = :tipo')
                        ->setParameter('estado', $estadoActivo->getId())
                        ->setParameter('tipo', 'R')
                        ->orderBy('p.nombre', 'ASC')
                    ;

                    $result = $sqlPlanchas->getQuery()->getResult();

                    if(!empty($result)){

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de planchas tipo R',
                            'data' => $result
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron planchas tipo R para listar',
                            'data' => 0
                        ];

                    }
  
                break;
               
            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function Actualizar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se actualiza el nombre de una plancha. Para realizar esta operación se solicita
            el id de una plancha en específica.
            -----------------------------------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obetener parámetros
            //===================

            $tipo = $request->get('tipo');
            $nombre = $request->get('nombre');
            $idPlancha = $request->get('idPlancha');

            if(!empty($idPlancha)){

                $plancha = $doctrine->getRepository(TPlanchas::class)->find($idPlancha);

                if(!empty($plancha)){

                    if(!empty($nombre)){

                        $nombre = $nombre;

                    }else{

                        $nombre = $plancha->getNombre();

                    }

                    if(!empty($tipo)){

                        $tipo = $tipo;

                    }else{

                        $tipo = $plancha->getTipo();

                    }

                    //Se actualiza la plancha
                    //========================

                    $plancha->setTipo($tipo);
                    $plancha->setNombre($nombre);
                    $em->persist($plancha);
                    $em->flush();

                    $data = [
                        'status' => 'success',
                        'code' => '200',
                        'message' => 'Plancha actualizada con éxito',
                        'data' => $plancha
                    ];

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontró la plancha con el id '.$idPlancha,
                        'data' => 0
                    ];

                }

            }else{

                $data = [
                    'status' => 'error',
                    'code' => '400',
                    'message' => 'Por favor ingrese un id de plancha'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }



        return $this->resjson($data);

    }

    public function UsuariosPlancha(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se listan los usuarios vinculados a una plancha.
            ---------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();
    
            //Obtener parámetro
            //=================

            $idPlancha = $request->get('idPlancha');

            if(!empty($idPlancha)){

                //Se verifica la existencia de la plancha
                //=======================================

                $plancha = $doctrine->getRepository(TPlanchas::class)->find($idPlancha);

                if(!empty($plancha)){

                    //Se obtienen los usuarios vinculado a la plancha
                    //===============================================

                    $usuariosPlancha = $doctrine->getRepository(TUsuariosPlancha::class)->findBy([
                        'plancha' => $plancha->getId()
                    ]);

                    if(!empty($usuariosPlancha)){

                        $index = 0;
                        $listUsuarios = [];

                        foreach($usuariosPlancha as $up){

                            $listUsuarios[$index] = $up;
                            $index ++;

                        }

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de usuarios vinculados a la plancha',
                            'data' => $listUsuarios
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron usuarios vinculados a esta plancha',
                            'data' => 0
                        ];

                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontró la plancha con el id '. $idPlancha,
                        'data' => 0
                    ];

                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese un id de plancha'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function ActualizarEstado(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se actualiza el estado de una plancha (Activo/Inactivo).
            -----------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();
    
            //Obtener parámetro
            //=================

            $estado = $request->get('estado');
            $idPlancha = $request->get('idPlancha');

            if(!empty($idPlancha)){

                $plancha = $doctrine->getRepository(TPlanchas::class)->find($idPlancha);

                if(!empty($plancha)){

                    if(!empty($estado)){

                        $estado_ = $doctrine->getRepository(TGeneral::class)->findOneBy([
                            'valor' => $estado
                        ]);
    
                        //Se actualiza el estado
                        //======================
    
                        $plancha->setEstado($estado_);
                        $em->persist($plancha);
                        $em->flush();
                        
                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Estado actualizado con éxito',
                            'data' => $plancha
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'Por favor ingrese un estado'
                        ];

                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontró la plancha con el id '.$idPlancha,
                        'data' => 0
                    ];

                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese un id de plancha'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }
    
}
