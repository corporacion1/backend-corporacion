<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\TGeneral;
use App\Services\JwtAuth;
use App\Entity\TConsultas;

class TConsultasController extends AbstractController
{
    
    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function Registrar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza el registro de consultas (Preguntas).
            ---------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Se obtiene parámetro
            //====================

            $nombre = $request->get('nombre');
            $descripcion = $request->get('descripcion');

            if(!empty($nombre)){

                if(!empty($descripcion)){

                    $descripcion = $descripcion;

                }else{

                    $descripcion = '';

                }

                //Registro de la consulta
                //=======================

                $consulta = new TConsultas();
                $consulta->setNombre($nombre);
                $consulta->setDescripcion($descripcion);
                $em->persist($consulta);
                $em->flush();

                $data = [
                    'status' => 'success',
                    'code' => '200',
                    'message' => 'Consulta registrada con éxito',
                    'data' => $consulta
                ];

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese un nombre'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function Listar(Request $request, PaginatorInterface $paginator, JwtAuth $jwt_auth, $opc){

        /*
           En este método se listan todas las consultas registradas en t_consultas.También se lista 
           la información de una consulta mediante un id especifico.
           ----------------------------------------------------------------------------------------
           CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
           
           $identity = $jwt_auth->checkToken($token, true);
           $doctrine = $this->getDoctrine();
           $em = $doctrine->getManager();

           //Obtenemos parámetro
           //===================

           $idConsulta = $request->get('idConsulta');
           $pagination = $request->get('pagination');

            switch($opc){

                case 1:

                    //Se obtiene la lista de consultas
                    //================================

                    $sqlConsultas = $em->createQueryBuilder()
                        ->select('c') 
                        ->from('App\Entity\TConsultas','c')
                        ->orderBy('c.id', 'DESC')
                    ;

                    $result = $sqlConsultas->getQuery()->getResult();

                    if(!empty($result)){

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de consultas',
                            'data' => $result
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron consultas para listar',
                            'data' => 0
                        ];

                    }
  
                break;

                case 2:

                    //Se lista la informacion de una consulta mediante un id en específico
                    //====================================================================

                    if(!empty($idConsulta)){

                        $consulta = $doctrine->getRepository(TConsultas::class)->find($idConsulta);

                        if(!empty($consulta)){
                            
                            $data = [
                                'status' => 'success',
                                'code' => '200',
                                'message' => 'Información de la consulta',
                                'data' => $consulta
                            ];

                        }else{

                            $data = [
                                'status' => 'success',
                                'code' => '300',
                                'message' => 'No se encontró la consulta con el id '.$idConsulta,
                                'data' => 0
                            ];

                        }

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'Por favor ingrese un id de consulta'
                        ];

                    }
                
                break;

                case 3:

                    //Se obtiene la lista de consutlas con paginator
                    //==============================================

                    $sqlConsultas = $em->createQueryBuilder()
                        ->select('c') 
                        ->from('App\Entity\TConsultas','c')
                        ->orderBy('c.id', 'DESC')
                    ;

                    $result = $sqlConsultas->getQuery()->getResult();

                    if(!empty($result)){

                        if(!empty($pagination)){

                            $page = $request->query->getInt('page', $pagination);
                            $items_per_page = 10;
                            $pagination = $paginator->paginate($result, $page, $items_per_page);
                            $total = $pagination->getTotalItemCount();

                            $consultas = [
                                'total_item_count' => $total,
                                'page_actual' => $page,
                                'items_per_page' => $items_per_page,
                                'total_pages' => ceil($total / $items_per_page),
                                'consultas' => $pagination
                            ];

                        }else{

                            $page = $request->query->getInt('page', 1);
                            $items_per_page = 10;
                            $pagination = $paginator->paginate($result, $page, $items_per_page);
                            $total = $pagination->getTotalItemCount();

                            $consultas = [
                                'total_item_count' => $total,
                                'page_actual' => $page,
                                'items_per_page' => $items_per_page,
                                'total_pages' => ceil($total / $items_per_page),
                                'consultas' => $pagination
                            ];

                        }

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de consultas',
                            'data' => $consultas
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron consultas para listar',
                            'data' => 0
                        ];

                    }

                break;
               
            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

}
