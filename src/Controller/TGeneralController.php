<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;                                
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\TGeneral;
use App\Services\JwtAuth;

class TGeneralController extends AbstractController
{
    
    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function ListarParametrizacion(Request $request, PaginatorInterface $paginator, JwtAuth $jwt_auth)
    {
        /*
            En este método se realizan todos los registros de t_general. La lista se genera
            mediante paginator.
            -------------------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $this->getDoctrine()->getManager();

            //Obtener parámetro
            //=================

            $pagination = $request->get('pagination');
    
            $sqlParametrizaciones = $em->createQueryBuilder()
                ->select('g') 
                ->from('App\Entity\TGeneral','g')
                ->orderBy('g.categoria, g.nombre', 'ASC')
            ;

            $result = $sqlParametrizaciones->getQuery()->getResult();

            if(!empty($result)){

                if(!empty($pagination)){

                    $page = $request->query->getInt('page', $pagination);
                    $items_per_page = 10;
                    $pagination = $paginator->paginate($result, $page, $items_per_page);
                    $total = $pagination->getTotalItemCount();

                    $parametrizaciones = [
                        'total_item_count' => $total,
                        'page_actual' => $page,
                        'items_per_page' => $items_per_page,
                        'total_pages' => ceil($total / $items_per_page),
                        'parametrizaciones' => $pagination
                    ];

                }else{

                    $page = $request->query->getInt('page', 1);
                    $items_per_page = 10;
                    $pagination = $paginator->paginate($result, $page, $items_per_page);
                    $total = $pagination->getTotalItemCount();

                    $parametrizaciones = [
                        'total_item_count' => $total,
                        'page_actual' => $page,
                        'items_per_page' => $items_per_page,
                        'total_pages' => ceil($total / $items_per_page),
                        'parametrizaciones' => $pagination
                    ];

                }

                $data = [
                    'status' => 'success',
                    'code' => '200',
                    'message' => 'Lista de parametrizaciones',
                    'data' => $parametrizaciones
                ];

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'No se encontraron registros para listar',
                    'data' => 0
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado',
                'data' => []
            ];

        }
        
        return $this->resjson($data);

    }

    public function ListarParametrizacionCategoriasPaginator(Request $request, PaginatorInterface $paginator, JwtAuth $jwt_auth)
    {
        /*
            En este metodo se realiza el listado de las parametrizaciones por categorias
            con paginacion por parte de los administradores.
            ----------------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if ($authCheck) {
            
            $identity = $jwt_auth->checkToken($token, true);

            $em = $this->getDoctrine()->getManager();

            // Parametros
            $pageNumero = $request->get('pagination');
            $categoria = $request->get('categoria');

            if(!empty($categoria)){

                $sqlParametrizacionCategorias = $em->createQueryBuilder()
                    ->select('gc')
                    ->from('App\Entity\TGeneral', 'gc')
                    ->where('gc.categoria = :categoria')
                    ->orderBy('gc.nombre', 'ASC')
                    ->setParameter('categoria', $categoria);

                if($sqlParametrizacionCategorias != null){

                    if($pageNumero == 0 Or $pageNumero == null){

                        $pageNumero = 1;
                        $page = $request->query->getInt('page', $pageNumero);
                        $items_per_page = 10;
                        $pagination = $paginator->paginate($sqlParametrizacionCategorias, $page, $items_per_page);
                        $total = $pagination->getTotalItemCount();

                        $data = [
                            'status' => 'exito',
                            'code'  => '200',
                            'message' => 'Lista de registros de parametrizaciones por la categoria '.$categoria,
                            'total_item_count' => $total,
                            'page_actual' => $page,
                            'items_per_page' => $items_per_page,
                            'total_pages' => ceil($total / $items_per_page),
                            'parametrizaciones' => $pagination
                        ];

                    }else{

                        $page = $request->query->getInt('page', $pageNumero);
                        $items_per_page = 10;
                        $pagination = $paginator->paginate($sqlParametrizacionCategorias, $page, $items_per_page);
                        $total = $pagination->getTotalItemCount();

                        $data = [
                            'status' => 'exito',
                            'code'  => '200',
                            'message' => 'Lista de registros de parametrizaciones por la categoria '.$categoria,
                            'total_item_count' => $total,
                            'page_actual' => $page,
                            'items_per_page' => $items_per_page,
                            'total_pages' => ceil($total / $items_per_page),
                            'parametrizaciones' => $pagination
                        ];
                    }
                
                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontraron registros para listar',
                        'data' => []
                    ];
                }
            
            }else{

                $data = [
                    'status' => 'error',
                    'code' => '400',
                    'message' => 'Por favor complete los campos'
                ];
            }
        
        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado',
                'data' => []
            ];

        }
        
        return $this->resjson($data);
    }

    public function ListarParametrizacionCategorias(Request $request, JwtAuth $jwt_auth)
    {
        /*
            En este metodo se realiza el listado de las parametrizaciones por categorias
            sin paginacion por parte de los administradores.
            ----------------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if ($authCheck) {
            
            $identity = $jwt_auth->checkToken($token, true);

            $em = $this->getDoctrine()->getManager();

            // Parametros
            $categoria = $request->get('categoria');

            if(!empty($categoria)){

                $sqlParametrizacionCategorias = $em->createQueryBuilder()
                    ->select('gc')
                    ->from('App\Entity\TGeneral', 'gc')
                    ->where('gc.categoria = :categoria')
                    ->orderBy('gc.nombre', 'ASC')
                    ->setParameter('categoria', $categoria);

                if($sqlParametrizacionCategorias != null){

                    $resultParametrizacionCategorias = $sqlParametrizacionCategorias->getQuery()->getResult();

                    $data = [

                        'status' => 'exito',
                        'code'  => '200',
                        'message' => 'Lista de registros de parametrizaciones por la categoria '.$categoria,
                        'data' => $resultParametrizacionCategorias

                    ];
                
                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontraron registros para listar',
                        'data' => []
                    ];
                }
            
            }else{

                $data = [
                    'status' => 'error',
                    'code' => '400',
                    'message' => 'Por favor complete los campos'
                ];
            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado',
                'data' => []
            ];

        }
        
        return $this->resjson($data);
    }

    public function CrearParametrizacion(Request $request, JwtAuth $jwt_auth)
    {

        /* 
            En este metodo se realiza la creacion de los registros de parametrizacion por parte
            de los administradores.
            -----------------------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);
        
        if($authCheck){

            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parametros del json
            //===========================

            $json = $request->get('json', null);
            $params = json_decode($json);

            if(!empty($json)){

                $valor = !empty($params->valor) ? $params->valor : null;
                $nombre = !empty($params->nombre) ? $params->nombre : null;
                $categoria = !empty($params->categoria) ? $params->categoria : null;
                $descripcion = !empty($params->descripcion) ? $params->descripcion : null;

                if(!empty($descripcion)){

                    $descripcion = $descripcion;

                }else{

                    $descripcion = "Sin descripción";
                    
                }

                if(!empty($nombre) && !empty($valor) && !empty($categoria)){

                    $tGeneral = $doctrine->getRepository(TGeneral::class)->findOneBy([
                        'valor' => $valor
                    ]);

                    if(!empty($tGeneral)){

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'La parametrizacion con el valor '.$valor.' ya se encuentra registrada'
                        ];

                    }else{                    

                        $general = new TGeneral();
                        $general->setNombre($nombre);
                        $general->setValor($valor);
                        $general->setCategoria($categoria);
                        $general->setDescripcion($descripcion);

                        $em->persist($general);
                        $em->flush();

                        $data = [

                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Parametrizacion registrada con éxito',
                            'data' => $general
                        
                        ];

                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'Por favor complete todos los campos'
                    ];

                }

            }else{

                $data = [
                    'status' => 'error',
                    'code' => '400',
                    'message' => 'Json vacío'
                ];

            }

        } else {
            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado',
                'data' => []
            ];
        }

        return $this->resjson($data);

    }

    public function CategoriasLike(Request $request, JwtAuth $jwt_auth){

        /*
            En este método, se listan todas las categorias de la tabla general. Mediante el 
            parametro categoria, se realiza un like el cual listará todas las categorias
            que cumplan con el parametro de busqueda. Si categoria es vacio se listarán
            todas las categorias existente en la base de datos.
            ------------------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if ($authCheck) {
            
            $identity = $jwt_auth->checkToken($token, true);

            $em = $this->getDoctrine()->getManager();
            $doctrine = $this->getDoctrine();
    
            //Obtenemos parámetros
            //====================
    
            $categoria = $request->get('categoria');
    
            if(empty($categoria)){
    
                $sqlCategorias = $em->createQueryBuilder()
                    ->select('g.categoria')
                    ->from('App\Entity\TGeneral', 'g')
                    ->groupBy('g.categoria')
                    ->orderBy('g.categoria')
                ;
                                
                $result = $sqlCategorias->getQuery()->getResult();
    
                if(!empty($result)){
    
                    $data = [
    
                        'status' => 'success',
                        'code' => '200',
                        'message' => 'Lista de categorías',
                        'data' => $result
            
                    ];
    
                }else{
    
                    $data = [
    
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontraron categorías para listar',
                        'data' => []
            
                    ];
    
                }
    
            }else{
    
                $sqlCategorias = $em->createQueryBuilder()
                    ->select('g.categoria')
                    ->from('App\Entity\TGeneral', 'g')
                    ->where('g.categoria LIKE :categoria')
                    ->setParameter('categoria', '%'.$categoria.'%')
                    ->groupBy('g.categoria')
                    ->orderBy('g.categoria')
                ;
                                
                $result = $sqlCategorias->getQuery()->getResult();
    
                if(empty($result)){
    
                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontraron categorías que coincidan con el parámetro de búsqueda',
                        'data' => 0
                    ];
    
                }else{
    
                    $data = [
                        'status' => 'success',
                        'code' => '200',
                        'message' => 'Lista de categorías',
                        'data' => $result
                    ];
    
                }
    
            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado',
                'data' => []
            ];
            
        }   

        return $this->resjson($data);

    }

    public function ListarCategorias(Request $request, JwtAuth $jwt_auth){

        /*
            En este método, se listan todas las categorías por nombre de categoría.
            -----------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $em = $this->getDoctrine()->getManager();
            $doctrine = $this->getDoctrine();

            //Se obtiene la lista de las categorías
            //=====================================

            $sqlCategorias = $em->createQueryBuilder()
                ->select('g')
                ->from('App\Entity\TGeneral', 'g')
                ->orderBy('g.categoria', 'ASC')
            ;

            $result = $sqlCategorias->getQuery()->getResult();

            if(!empty($result)){

                $index = 0;
                $indexCategoria = 0;
                $listCategorias = [];
                $listCategoriasUnique = [];

                //Se obtienen las categorías sin duplicados
                //=========================================

                foreach($result as $r){

                    $listCategorias[$index] = $r->getCategoria();
                    $index ++;

                }

                $listCategorias = array_unique($listCategorias);

                foreach($listCategorias as $lc){

                    $listCategoriasUnique[$indexCategoria] = $lc;
                    $indexCategoria ++;

                }

                $data = [
                    'status' => 'success',
                    'code' => '200',
                    'message' => 'Lista de categorías',
                    'data' => $listCategoriasUnique
                ];

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'No se encontraron categorías para listar',
                    'data' => 0
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function ParametrizacionPorCategoria(Request $request, PaginatorInterface $paginator, JwtAuth $jwt_auth){

        /*
            En este método se retornan todas las parametrizaciones por categoría. Para realizar
            esta operación se solicita el nombre de la categoría.
            -----------------------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parámetro
            //=================

            $categoria = $request->get('categoria');
            $pagination = $request->get('pagination');

            if(!empty($categoria)){

                $sqlParametrizaciones = $em->createQueryBuilder()
                    ->select('g')
                    ->from('App\Entity\TGeneral', 'g')
                    ->where('g.categoria = :categoria')
                    ->setParameter('categoria', $categoria)
                    ->orderBy('g.nombre', 'ASC')
                ;

                $result = $sqlParametrizaciones->getQuery()->getResult();

                if(!empty($result)){

                    if(!empty($pagination)){

                        $page = $request->query->getInt('page', $pagination);
                        $items_per_page = 15;
                        $pagination = $paginator->paginate($result, $page, $items_per_page);
                        $total = $pagination->getTotalItemCount();

                        $parametrizaciones = [
                            'total_item_count' => $total,
                            'page_actual' => $page,
                            'items_per_page' => $items_per_page,
                            'total_pages' => ceil($total / $items_per_page),
                            'parametrizaciones' => $pagination
                        ];

                    }else{

                        $page = $request->query->getInt('page', 1);
                        $items_per_page = 15;
                        $pagination = $paginator->paginate($result, $page, $items_per_page);
                        $total = $pagination->getTotalItemCount();

                        $parametrizaciones = [
                            'total_item_count' => $total,
                            'page_actual' => $page,
                            'items_per_page' => $items_per_page,
                            'total_pages' => ceil($total / $items_per_page),
                            'parametrizaciones' => $pagination
                        ];

                    }

                    $data = [
                        'status' => 'success',
                        'code' => '200',
                        'message' => 'Lista de parametrizaciones',
                        'data' => $parametrizaciones
                    ];

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontraron registros para esta categoría',
                        'data' => 0
                    ];

                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese una categoría'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado',
                'data' => []
            ];

        }

        return $this->resjson($data);

    }

    public function ParametrizacionPorNombre(Request $request, PaginatorInterface $paginator, JwtAuth $jwt_auth){

        /*
            En este método se retorna la información de una parametrización. Para realizar esta 
            búsqueda se solicita el nombre de una parametrización.
            -----------------------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parámetro
            //=================

            $nombre = $request->get('nombre');
            $pagination = $request->get('pagination');

            if(!empty($nombre)){

                $sqlParametrizaciones = $em->createQueryBuilder()
                    ->select('g')
                    ->from('App\Entity\TGeneral', 'g')
                    ->where('g.nombre LIKE :nombre')
                    ->setParameter('nombre', '%'.$nombre.'%')
                    ->orderBy('g.nombre', 'ASC')
                ;

                $result = $sqlParametrizaciones->getQuery()->getResult();

                if(!empty($result)){

                    if(!empty($pagination)){

                        $page = $request->query->getInt('page', $pagination);
                        $items_per_page = 15;
                        $pagination = $paginator->paginate($result, $page, $items_per_page);
                        $total = $pagination->getTotalItemCount();

                        $parametrizaciones = [
                            'total_item_count' => $total,
                            'page_actual' => $page,
                            'items_per_page' => $items_per_page,
                            'total_pages' => ceil($total / $items_per_page),
                            'parametrizaciones' => $pagination
                        ];

                    }else{

                        $page = $request->query->getInt('page', 1);
                        $items_per_page = 15;
                        $pagination = $paginator->paginate($result, $page, $items_per_page);
                        $total = $pagination->getTotalItemCount();

                        $parametrizaciones = [
                            'total_item_count' => $total,
                            'page_actual' => $page,
                            'items_per_page' => $items_per_page,
                            'total_pages' => ceil($total / $items_per_page),
                            'parametrizaciones' => $pagination
                        ];

                    }

                    $data = [
                        'status' => 'success',
                        'code' => '200',
                        'message' => 'Lista de parametrizaciones',
                        'data' => $parametrizaciones
                    ];

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontraron registros para esta categoría',
                        'data' => 0
                    ];

                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese un nombre'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado',
                'data' => []
            ];

        }

        return $this->resjson($data);

    }

    public function Actualizar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se actualiza la información de una parametrización obtenida mediante un id
            en específico.
            -----------------------------------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();
    
            //Obtener parametro
            //==================
    
            $idParametrizacion = $request->get('idParametrizacion');

            if(!empty($idParametrizacion)){

                $parametrizacion = $doctrine->getRepository(TGeneral::class)->find($idParametrizacion);

                if(!empty($parametrizacion)){

                    //Obtener parametros del json
                    //===========================
            
                    $json = $request->get('json', null);
                    $params = json_decode($json);
    
                    if($json != null){

                        $valor = !empty($params->valor) ? $params->valor : $parametrizacion->getValor();
                        $nombre = !empty($params->nombre) ? $params->nombre : $parametrizacion->getNombre();
                        $categoria = !empty($params->categoria) ? $params->categoria : $parametrizacion->getCategoria();
                        $descripcion = !empty($params->descripcion) ? $params->descripcion : $parametrizacion->getDescripcion();

                        //Se valida la existencia del valor
                        //=================================

                        if(!empty($valor)){

                            if($valor == $parametrizacion->getValor()){

                                $estadoValor = 1;
                                $valor = $parametrizacion->getValor();

                            }else{

                                $parametrizacionUnique = $doctrine->getRepository(TGeneral::class)->findOneBy([
                                    'valor' => $valor
                                ]);

                                if(!empty($parametrizacionUnique)){
                                    
                                    $estadoValor = 0;

                                }else{

                                    $estadoValor = 1;
                                    $valor = $valor;

                                }

                            }

                        }else{

                            $estadoValor = 1;
                            $valor = $parametrizacion->getValor();

                        }

                        //Se valida la existencia del nombre
                        //==================================

                        if(!empty($nombre)){

                            if($nombre == $parametrizacion->getNombre()){

                                $estadoNombre = 1;
                                $nombre = $parametrizacion->getNombre();

                            }else{

                                $parametrizacionUnique = $doctrine->getRepository(TGeneral::class)->findOneBy([
                                    'nombre' => $nombre
                                ]);

                                if(!empty($parametrizacionUnique)){
                                    
                                    $estadoNombre = 0;

                                }else{

                                    $estadoNombre = 1;
                                    $nombre = $nombre;

                                }

                            }

                        }else{

                            $estadoNombre = 1;
                            $nombre = $parametrizacion->getNombre();

                        }

                        //Se validan los campos únicos
                        //============================

                        if($estadoValor == 1 && $estadoNombre == 1){

                            $parametrizacion->setValor($valor);
                            $parametrizacion->setNombre($nombre);
                            $parametrizacion->setCategoria($categoria);
                            $parametrizacion->setDescripcion($descripcion);

                            $em->persist($parametrizacion);
                            $em->flush();

                            $data = [
                                'status' => 'success',
                                'code' => '200',
                                'message' => 'Parametrización actualizada con éxito',
                                'data' => $parametrizacion
                            ];

                        }elseif($estadoValor == 0){

                            $data = [
                                'status' => 'success',
                                'code' => '300',
                                'message' => 'Ya existe una parametrización con este valor'
                            ];

                        }elseif($estadoNombre == 0){

                            $data = [
                                'status' => 'success',
                                'code' => '300',
                                'message' => 'Ya existe una parametrización con este nombre'
                            ];

                        }

                    }else{

                        $data = [
                            'status' => 'error',
                            'code' => '400',
                            'message' => 'Json vacío'
                        ];

                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontró la parametrización con el id '.$idParametrizacion,
                        'data' => 0
                    ];

                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese un id de parametriación'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

}
