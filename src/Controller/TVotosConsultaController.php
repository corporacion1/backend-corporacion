<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\TGeneral;
use App\Services\JwtAuth;
use App\Entity\TUsuarios;
use App\Entity\TApoderado;
use App\Entity\TVotosConsulta;
use App\Entity\TRespuestasConsulta;

class TVotosConsultaController extends AbstractController
{

    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function Registrar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se resliza el registro de votaciones.
            ----------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Se obtiene parámetro
            //====================

            $idRespuesta = $request->get('idRespuesta');

            $fecha = new \DateTime('now', new \DateTimeZone('America/Bogota'));
            $fechaVotacion = $fecha->format('Y-m-d H:i:s');

            $apoderado = $doctrine->getRepository(TUsuarios::class)->find($identity->sub);

            //Se obtiene el sistema operativo, el navegador y la versión
            //==========================================================

            $os = ["WIN","MAC","LINUX"];
            $browser = ["IE","OPERA","MOZILLA","NETSCAPE","FIREFOX","SAFARI","CHROME"];
        
            $info['os'] = "OTHER";
            $info['browser'] = "OTHER";
            $info['version'] = "OTHER";
        
            foreach($browser as $parent){

                $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
                $f = $s + strlen($parent);
                $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 15);
                $version = preg_replace('/[^0-9,.]/','',$version);

                if($s){

                    $info['browser'] = $parent;
                    $info['version'] = $version;

                }
            }
        
            foreach($os as $val){

                if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']),$val)!==false){

                    $info['os'] = $val;

                }

            }

            if(!empty($idRespuesta)){

                $respuesta = $doctrine->getRepository(TRespuestasConsulta::class)->find($idRespuesta);

                if(!empty($respuesta) && $respuesta->getEstado()->getValor() == 'A'){

                    //Se verifica si el apoderado ya tiene un voto registrado
                    //=======================================================

                    $votoApoderado = $doctrine->getRepository(TVotosConsulta::class)->findOneBy([
                        'apoderado' => $identity->sub
                    ]);

                    if(empty($votoApoderado)){

                        $voto = new TVotosConsulta();
                        $voto->setSO($info['os']);
                        $voto->setRespuesta($respuesta);
                        $voto->setApoderado($apoderado);
                        $voto->setFechaVoto($fechaVotacion);
                        $voto->setIpComputo($_SERVER['REMOTE_ADDR']);
                        $voto->setNavegador($info['browser'].' - '.$info['version']);
    
                        $em->persist($voto);
                        $em->flush();
    
                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Voto registrado con éxito',
                            'data' => $voto
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'Este apoderado ya tiene registrado un voto'
                        ];

                    }
    
                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontró la respuesta con el id '.$idRespuesta,
                        'data' => 0
                    ];

                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese un id de respuesta'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function VotacionesRespuesta(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se contabiliza el total de votaciones por plancha.
            -----------------------------------------------------------------
            CORPOSOFT
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            $estadoActivo = $doctrine->getRepository(TGeneral::class)->findOneBy([
                'valor' => 'A'
            ]);

            //Se obtienen todas las respuestas activas
            //========================================

            $sqlClientes = $em->createQueryBuilder()
                ->select('r')
                ->from('App\Entity\TRespuestasConsulta', 'r')
                ->leftJoin('App\Entity\TGeneral','g',
                    \Doctrine\ORM\Query\Expr\Join::WITH,
                    'r.opcion = g.id')
                ->where('g.categoria = :categoria AND r.estado = :estado')
                ->setParameter('categoria', 'Tipo Consulta')
                ->setParameter('estado', $estadoActivo->getId())
                ->orderBy('r.id', 'DESC') 
            ;

            $respuestas = $sqlClientes->getQuery()->getResult();

            if(!empty($respuestas)){

                //Se obtienen los votos de cada respuesta
                //=======================================

                $index = 0;
                $totalVotos = 0;
                $listRespuestas = [];

                foreach($respuestas as $p){

                    $votos = $doctrine->getRepository(TVotosConsulta::class)->findBy([
                        'respuesta' => $p->getId()
                    ]);

                    if(!empty($votos)){

                        //Se obtienen el total de votos por respuesta
                        //===========================================

                        foreach($votos as $v){

                            $totalVotos ++;
    
                        }

                    }else{

                        $totalVotos = 0;

                    }

                    $dataRespuestas = [
                        'id' => $p->getId(),
                        'respuesta' => $p->getOpcion()->getNombre(),
                        'totalVotos' => $totalVotos
                    ];

                    $listRespuestas[$index] = $dataRespuestas;
                    $totalVotos = 0;
                    $index ++;

                }

                $data = [
                    'status' => 'success',
                    'code' => '200',
                    'message' => 'Total votos por respuesta',
                    'data' => $listRespuestas
                ];

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'No se encontraron respuestas activas para listar',
                    'data' => 0
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

}
