<?php

namespace App\Services;
use Doctrine\ORM\EntityManagerInterface;

use Firebase\JWT\JWT;
use App\Entity\TUsuarios;

class SendEmail
{

    public function sendEmail($valuesEmail){

        /*
            En este método se realiza el envío de correos al momento de confirmar usuarios.
            -------------------------------------------------------------------------------
            CORPOSOFT
        */

        //Email Registro de usuarios
        //==========================

        $message = (new \Swift_Message());
        $message->setSubject('REGISTRO DE USUARIOS');
        $message->setFrom([$valuesEmail[1] => 'CORPOSOFT']);
        $message->setTo([$valuesEmail[2] => 'Usuario']);
        $message->setEncoder( new \Swift_Mime_ContentEncoder_PlainContentEncoder('8bit'));

        //Validación tipo de imagen
        //=========================

        $colorTable = '#020040';
        $msgEmpresa = 'CORPOSOFT';

        $img_attachment1 = \Swift_Image::fromPath($valuesEmail[3].'user.png');
        $img_attachment2 = \Swift_Image::fromPath($valuesEmail[3].'key.png');
        $image1 = $message->embed($img_attachment1);        
        $image2 = $message->embed($img_attachment2);        

        $template = '

            <center>

                <h2 style="color: #323230;
                    font-family: "Roboto Slab", serif;
                    margin-top: -15px;">¡HOLA '.$valuesEmail[6].'!</h2>
        
                <h3 style="color: #323230;
                    font-family: Verdana;
                    margin-top: -15px;">Mediante las siguientes credenciales podrás acceder al sistema</h3>
                    
                <br>
                
                <table style="border-radius: 10px 10px 10px 10px;
                    -moz-border-radius: 10px 10px 10px 10px;
                    -webkit-border-radius: 10px 10px 10px 10px;
                    border: 2px solid black; border-collapse: separate; border-spacing:0;">
        
                    <tr style="border: 2px solid black;
                        height: 50px;
                        background: #000000;">
                        <td colspan="2" style="font-family: Verdana;
                            font-size: 15px; color: #E6E7E6; border-radius: 7px 7px 1px 1px;
                            -moz-border-radius: 7px 7px 1px 1px;
                            -webkit-border-radius: 7px 7px 1px 1px;"><b><center>INFORMACIÓN DE ACCESO</center></b></td>
                    </tr>
        
                    <tr style="border: 1px solid black;
                        height: 35px;
                        background-color: #A9ABAE;">
        
                        <td style="font-family: Verdana;
                            font-size: 14px;
                            border: 1px solid black; width: 200px;"><b><center>Usuario <img src="'.$image1.'" width="12px"></center></b></td>
        
                        <td style="font-family: Verdana;
                            font-size: 14px;
                            border: 1px solid black; width: 200px;"><b><center>Contraseña <img src="'.$image2.'" width="12px"></center></b></td>
        
                    </tr>
        
                    <tr style="border: 1px solid black;
                        height: 35px;
                        background-color: #E6E7E6;">
        
                        <td style="font-family: Verdana;
                            font-size: 14px;
                            border: 1px solid black; border-radius: 0px 0px 0px 7px;
                            -moz-border-radius: 0px 0px 0px 7px;
                            -webkit-border-radius: 0px 0px 0px 7px;">
                            <center>'.$valuesEmail[4].'</center>
                        </td>
        
                        <td style="font-family: Verdana;
                            font-size: 14px;
                            border: 1px solid black; border-radius: 0px 0px 7px 0px;
                            -moz-border-radius: 0px 0px 7px 0px;
                            -webkit-border-radius: 0px 0px 7px 0px;">
                            <center>'.$valuesEmail[5].'</center>
                        </td>
        
                    </tr>
        
                </table>    
        
            </center>
        
        ';       

        $message->addPart(
            $template,
            'text/html'
        );

        if($valuesEmail[0]->send($message)) {

            $dataEmail = $valuesEmail[7];

            $status = 'success';
            $code = '200';
            $mensaje = 'Usuario registrado con éxito, hemos enviado un correo a ' . $valuesEmail[2] . ' con tus credencial para acceder al sistema';

        }else{

            $dataEmail = [
                'correoSend'  => false,
                'correoMsg' => 'El correo no se ha enviado'
            ];

            $status = 'success';
            $code = '300';
            $mensaje = 'Ha ocurrido un error al enviar el correo';
            
        }

        $data = [

            'status' => $status,
            'code' => $code,
            'message' => $mensaje,
            'data' => $dataEmail
        ];

        return $data;

    }
}
