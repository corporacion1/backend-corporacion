<?php

namespace App\Services;
use Doctrine\ORM\EntityManagerInterface;

use Firebase\JWT\JWT;
use App\Entity\TUsuarios;
use App\Entity\TSesiones;

class JwtAuth
{

    public $entityManager;
    private $repositoryIngreso;

    public $manager;
    public $key = 'Corporacion_02022021';

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repositoryUsuarios = $entityManager->getRepository(TUsuarios::class);
        $this->repositorySesiones = $entityManager->getRepository(TSesiones::class);
    }

    public function signup($usuario, $password, $gettoken = null){

        /*
            En este método se valida el acceso a sistema mediante un usuario y una contraseña.
            ----------------------------------------------------------------------------------
            CORPOSOFT
        */

        $fecha = new \DateTime('now', new \DateTimeZone('America/Bogota'));
        $fechaInicioSesion = $fecha->format('Y-m-d H:i:s');

        $fechaToken = time() + (7 * 24 * 60 * 60);
        $fechaExpiracionToken= date('Y-m-d H:i:s', $fechaToken);

        //Se obtiene la ip y el navegador donde se inicio sesión desde el dispositivo
        //===========================================================================

        $browser = ["IE","OPERA","MOZILLA","NETSCAPE","FIREFOX","SAFARI","CHROME"];
        
        $info['browser'] = "OTHER";
        $info['version'] = "OTHER";
    
        foreach($browser as $parent){

            $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
            $f = $s + strlen($parent);
            $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 15);
            $version = preg_replace('/[^0-9,.]/','',$version);

            if($s){

                $info['browser'] = $parent;
                $info['version'] = $version;

            }
        }

        //Se verifica la existencia del usuario
        //=====================================

        $pwd = hash('sha256', $password);

        $usuario = $this->repositoryUsuarios->findOneBy([
            'usuario' => $usuario,
            'contraseina' => $pwd
        ]);

        if(!empty($usuario)){

            $token = [
                'sub' => $usuario->getId(),
                'nombres' => $usuario->getNombres(),
                'apellidos' => $usuario->getApellidos(),
                'tipoDocumento' => $usuario->getTipoDocumento(),
                'documento' => $usuario->getDocumento(),
                'correo' => $usuario->getCorreo(),
                'rol' => $usuario->getRol(),
                'estado' => $usuario->getEstado(),
                'iat' => time(),
                'exp' => time() + (7 * 24 * 60 * 60)
            ];

            //Token encriptado
            //================

            $jwt = JWT::encode($token, $this->key, 'HS256');

            if(!empty($gettoken)){

                $data = $jwt;

            }else{

                //Se valida si el usuario tiene una sesión iniciada en otro dispositivo
                //=====================================================================

                $sesion = $this->repositorySesiones->findOneBy([
                    'usuario' => $usuario->getId(),
                    'fechaCierreSesion' => '0000-00-00'
                ]);

                if(empty($sesion)){

                    //Token desencriptado
                    //===================
    
                    $decoded = JWT::decode($jwt, $this->key, ['HS256']);
                    $data = $decoded;

                    $dataLogin = [
                        'sesion' => false,
                        'data' => $decoded
                    ];
    
                    $data = [
                        'status' => 'success',
                        'message' => 'Se ha iniciado sesión correctamente',
                        'data' => $dataLogin
                    ];

                    //Se registra un nuevo inicio de sesión
                    //=====================================

                    $sesion = new TSesiones();
                    $sesion->setUsuario($usuario);
                    $sesion->setFechaCierreSesion('0000-00-00');
                    $sesion->setIpComputo($_SERVER['REMOTE_ADDR']);
                    $sesion->setFechaInicioSesion($fechaInicioSesion);
                    $sesion->setFechaExpiracionToken($fechaExpiracionToken);
                    $sesion->setNavegador($info['browser'].' - '.$info['version']);

                    $this->entityManager->persist($sesion);
                    $this->entityManager->flush();

                }else{

                    $dataSesion = [
                        'ip' => $sesion->getIpComputo(),
                        'navegador' => $sesion->getNavegador(),
                        'idUsuario' => $usuario->getId()
                    ];
    
                    $dataLogin = [
                        'sesion' => true,
                        'data' => $dataSesion
                    ];
    
                    $data = [
                        'status' => 'success',
                        'message' => 'Cierre las sesiones inciadas en otros dispositivos para continuar',
                        'data' => $dataLogin
                    ];        

                }  

                $data = base64_encode(json_encode($data));

            }

        }else{

            $data = [
                'status' => 'error',
                'message' => 'El usuario o la contraseña ingresados son incorrectos'
            ];

            $data = base64_encode(json_encode($data));

        }

        return $data;

    }

    public function checkToken($jwt, $identity = false)
    {
        $auth = false;
        try {
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);
        } catch (\UnexpectedValueException $e) {
            $auth = false;
        } catch (\DomainException $e) {
            $auth = false;
        }
        if (isset($decoded) && !empty($decoded) && is_object($decoded) && isset($decoded->sub)) {
            $auth = true;
        } else {
            $auth = false;
        }
        if ($identity != false) {
            return $decoded;
        } else {
            return $auth;
        }
    }
}
