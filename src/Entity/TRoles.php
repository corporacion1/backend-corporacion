<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TRoles
 *
 * @ORM\Table(name="t_roles")
 * @ORM\Entity
 */
class TRoles implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="t_roles_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="string", nullable=true)
     */
    private $valor;

    //Relación con otras entidades
    //============================

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TUsuarios", mappedBy="rol")
     * @ORM\OneToMany(targetEntity="App\Entity\TRegistro", mappedBy="rol")
    */

    private $usuario;
    private $registro;

    public function __construct(){
       $this->usuario = new ArrayCollection();
       $this->registro = new ArrayCollection();
   }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getValor(): ?string
    {
        return $this->valor;
    }

    public function setValor(?string $valor): self
    {
        $this->valor = $valor;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'valor' => $this->valor,
            'nombre' => $this->nombre
        ];

    }

}
