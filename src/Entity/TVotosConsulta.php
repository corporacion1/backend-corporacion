<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TVotosConsulta
 *
 * @ORM\Table(name="t_votos_consulta", indexes={@ORM\Index(name="fki_t_votos_consulta_usuario_id_fkey", columns={"apoderado_id"}), @ORM\Index(name="fki_t_votos_consulta_respuesta_id_fkey", columns={"respuesta_id"})})
 * @ORM\Entity
 */
class TVotosConsulta implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="t_votos_consulta_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fecha_voto", type="string", nullable=true)
     */
    private $fechaVoto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip_computo", type="string", nullable=true)
     */
    private $ipComputo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sistema_operativo", type="string", nullable=true)
     */
    private $so;

    /**
     * @var string|null
     *
     * @ORM\Column(name="navegador", type="text", nullable=true)
     */
    private $navegador;

    /**
     * @var \TUsuarios
     *
     * @ORM\ManyToOne(targetEntity="TUsuarios", inversedBy="votoConsulta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="apoderado_id", referencedColumnName="id")
     * })
     */
    private $apoderado;

    /**
     * @var \TRespuestasConsulta
     *
     * @ORM\ManyToOne(targetEntity="TRespuestasConsulta", inversedBy="votoConsulta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="respuesta_id", referencedColumnName="id")
     * })
     */
    private $respuesta;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaVoto(): ?string
    {
        return $this->fechaVoto;
    }

    public function setFechaVoto(?string $fechaVoto): self
    {
        $this->fechaVoto = $fechaVoto;

        return $this;
    }

    public function getIpComputo(): ?string
    {
        return $this->ipComputo;
    }

    public function setIpComputo(?string $ipComputo): self
    {
        $this->ipComputo = $ipComputo;

        return $this;
    }

    public function getSO(): ?string
    {
        return $this->so;
    }

    public function setSO(?string $so): self
    {
        $this->so = $so;

        return $this;
    }

    public function getNavegador(): ?string
    {
        return $this->navegador;
    }

    public function setNavegador(?string $navegador): self
    {
        $this->navegador = $navegador;

        return $this;
    }

    public function getApoderado(): ?TUsuarios
    {
        return $this->apoderado;
    }

    public function setApoderado(?TUsuarios $apoderado): self
    {
        $this->apoderado = $apoderado;

        return $this;
    }

    public function getRespuesta(): ?TRespuestasConsulta
    {
        return $this->respuesta;
    }

    public function setRespuesta(?TRespuestasConsulta $respuesta): self
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'fechaVoto' => $this->fechaVoto,
            'ip' => $this->ipComputo,
            'so' => $this->so,
            'navegador' => $this->navegador,
            'apoderado' => $this->apoderado,
            'consulta' => $this->respuesta
        ];

    }

}
