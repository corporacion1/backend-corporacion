<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TApoderado
 *
 * @ORM\Table(name="t_apoderado", indexes={@ORM\Index(name="fki_t_apoderado_accionista_id_fkey", columns={"accionista_id"}), @ORM\Index(name="fki_t_apoderado_apoderado_id_fkey", columns={"apoderado_id"})})
 * @ORM\Entity
 */
class TApoderado implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="t_apoderado_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \TUsuarios
     *
     * @ORM\ManyToOne(targetEntity="TUsuarios", inversedBy="apoderado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="apoderado_id", referencedColumnName="id")
     * })
     */
    private $apoderado;

    /**
     * @var \TUsuarios
     *
     * @ORM\ManyToOne(targetEntity="TUsuarios", inversedBy="apoderado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="accionista_id", referencedColumnName="id")
     * })
     */
    private $accionista;

    //Relación con otras entidades
    //============================

    /*
     * @ORM\OneToMany(targetEntity="App\Entity\TVotos", mappedBy="apoderado")
    */

    private $votos;

    public function __construct(){
       $this->votos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApoderado(): ?TUsuarios
    {
        return $this->apoderado;
    }

    public function setApoderado(?TUsuarios $apoderado): self
    {
        $this->apoderado = $apoderado;

        return $this;
    }

    public function getAccionista(): ?TUsuarios
    {
        return $this->accionista;
    }

    public function setAccionista(?TUsuarios $accionista): self
    {
        $this->accionista = $accionista;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'apoderado' => $this->apoderado,
            'accionista' => $this->accionista
        ];

    }

}
