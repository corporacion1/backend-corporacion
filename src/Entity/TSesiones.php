<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TSesiones
 *
 * @ORM\Table(name="t_sesiones", indexes={@ORM\Index(name="fki_t_sesiones_usuario_id_fkey", columns={"usuario_id"})})
 * @ORM\Entity
 */
class TSesiones implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="t_sesiones_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fecha_inicio_sesion", type="string", nullable=true)
     */
    private $fechaInicioSesion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fecha_cierre_sesion", type="string", nullable=true)
     */
    private $fechaCierreSesion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fecha_expiracion_token", type="string", nullable=true)
     */
    private $fechaExpiracionToken;

     /**
     * @var string|null
     *
     * @ORM\Column(name="ip_computo", type="string", nullable=true)
     */
    private $ipComputo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="navegador", type="string", nullable=true)
     */
    private $navegador;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacion", type="string", nullable=true)
     */
    private $observacion;

    /**
     * @var \TUsuarios
     *
     * @ORM\ManyToOne(targetEntity="TUsuarios", inversedBy="sesion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaInicioSesion(): ?string
    {
        return $this->fechaInicioSesion;
    }

    public function setFechaInicioSesion(?string $fechaInicioSesion): self
    {
        $this->fechaInicioSesion = $fechaInicioSesion;

        return $this;
    }

    public function getFechaCierreSesion(): ?string
    {
        return $this->fechaCierreSesion;
    }

    public function setFechaCierreSesion(?string $fechaCierreSesion): self
    {
        $this->fechaCierreSesion = $fechaCierreSesion;

        return $this;
    }

    public function getFechaExpiracionToken(): ?string
    {
        return $this->fechaExpiracionToken;
    }

    public function setFechaExpiracionToken(?string $fechaExpiracionToken): self
    {
        $this->fechaExpiracionToken = $fechaExpiracionToken;

        return $this;
    }

    public function getIpComputo(): ?string
    {
        return $this->ipComputo;
    }

    public function setIpComputo(?string $ipComputo): self
    {
        $this->ipComputo = $ipComputo;

        return $this;
    }

    public function getNavegador(): ?string
    {
        return $this->navegador;
    }

    public function setNavegador(?string $navegador): self
    {
        $this->navegador = $navegador;

        return $this;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }

    public function getUsuario(): ?TUsuarios
    {
        return $this->usuario;
    }

    public function setUsuario(?TUsuarios $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'usuario' => $this->usuario,
            'fechaInicioSesion' => $this->fechaInicioSesion,
            'fechaCierreSesion' => $this->fechaCierreSesion,
            'fechaExpiracionToken' => $this->fechaExpiracionToken,
            'ip' => $this->ipComputo,
            'navegador' => $this->navegador,
            'observacion' => $this->observacion
        ];

    }

}
