<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TRespuestasConsulta
 *
 * @ORM\Table(name="t_respuestas_consulta", indexes={@ORM\Index(name="fki_t_respuestas_consulta_opcion_id_fkey", columns={"opcion_id"})})
 * @ORM\Entity
 */
class TRespuestasConsulta implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="t_respuestas_consulta_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \TConsultas
     *
     * @ORM\ManyToOne(targetEntity="TConsultas", inversedBy="respuesta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="consulta_id", referencedColumnName="id")
     * })
     */
    private $consulta;

    /**
     * @var \TGeneral
     *
     * @ORM\ManyToOne(targetEntity="TGeneral")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="opcion_id", referencedColumnName="id")
     * })
     */
    private $opcion;

    /**
     * @var \TGeneral
     *
     * @ORM\ManyToOne(targetEntity="TGeneral", inversedBy="respuesta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado_id", referencedColumnName="id")
     * })
     */
    private $estado;

    //Relación con otras entidades
    //============================

    /*
    * @ORM\OneToMany(targetEntity="App\Entity\TVotosConsulta", mappedBy="respuesta")
    */

    private $votoConsulta;

    public function __construct(){
       $this->votoConsulta = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConsulta(): ?TConsultas
    {
        return $this->consulta;
    }

    public function setConsulta(?TConsultas $consulta): self
    {
        $this->consulta = $consulta;

        return $this;
    }

    public function getOpcion(): ?TGeneral
    {
        return $this->opcion;
    }

    public function setOpcion(?TGeneral $opcion): self
    {
        $this->opcion = $opcion;

        return $this;
    }

    public function getEstado(): ?TGeneral
    {
        return $this->estado;
    }

    public function setEstado(?TGeneral $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'opcion' => $this->opcion,
            'consulta' => $this->consulta,
            'estado' => $this->estado
        ];

    }

}
