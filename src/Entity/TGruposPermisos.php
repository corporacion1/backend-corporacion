<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TGruposPermisos
 *
 * @ORM\Table(name="t_grupos_permisos", indexes={@ORM\Index(name="fki_t_grupos_permisos", columns={"permiso_id"}), @ORM\Index(name="fki_t_grupos_permisos_grupo_id_fkey", columns={"grupo_id"})})
 * @ORM\Entity
 */
class TGruposPermisos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="t_grupos_permisos_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", nullable=true)
     */
    private $nombre;

    /**
     * @var \TGrupos
     *
     * @ORM\ManyToOne(targetEntity="TGrupos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grupo_id", referencedColumnName="id")
     * })
     */
    private $grupo;

    /**
     * @var \TPermisos
     *
     * @ORM\ManyToOne(targetEntity="TPermisos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="permiso_id", referencedColumnName="id")
     * })
     */
    private $permiso;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getGrupo(): ?TGrupos
    {
        return $this->grupo;
    }

    public function setGrupo(?TGrupos $grupo): self
    {
        $this->grupo = $grupo;

        return $this;
    }

    public function getPermiso(): ?TPermisos
    {
        return $this->permiso;
    }

    public function setPermiso(?TPermisos $permiso): self
    {
        $this->permiso = $permiso;

        return $this;
    }


}
