<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TPlanchas
 *
 * @ORM\Table(name="t_planchas")
 * @ORM\Entity
 */
class TPlanchas implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="t_planchas_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo_plancha", type="string", nullable=true)
     */
    private $tipo;

    /**
     * @var \TGeneral
     *
     * @ORM\ManyToOne(targetEntity="TGeneral", inversedBy="plancha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado_id", referencedColumnName="id")
     * })
     */
    private $estado;

    //Relación con otras entidades
    //============================

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TVotos", mappedBy="plancha")
     * @ORM\OneToMany(targetEntity="App\Entity\TUsuariosPlancha", mappedBy="plancha")
    */

    private $votos;
    private $plancha;

    public function __construct(){
       $this->votos = new ArrayCollection();
       $this->plancha = new ArrayCollection();
   }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(?string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getEstado(): ?TGeneral
    {
        return $this->estado;
    }

    public function setEstado(?TGeneral $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'estado' => $this->estado,
            'tipo' => $this->tipo
        ];

    }

}
