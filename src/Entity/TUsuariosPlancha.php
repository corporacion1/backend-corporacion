<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * TUsuariosPlancha
 *
 * @ORM\Table(name="t_usuarios_plancha", indexes={@ORM\Index(name="fki_t_usuarios_plancha_usuario_id_fkey", columns={"usuario_id"}), @ORM\Index(name="fki_t_usuarios_plancha_plancha_id_fkey", columns={"plancha_id"})})
 * @ORM\Entity
 */
class TUsuariosPlancha implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="t_usuarios_plancha_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo", type="string", nullable=true)
     */
    private $tipo;

    /**
     * @var \TPlanchas
     *
     * @ORM\ManyToOne(targetEntity="TPlanchas", inversedBy="plancha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plancha_id", referencedColumnName="id")
     * })
     */
    private $plancha;

    /**
     * @var \TUsuarios
     *
     * @ORM\ManyToOne(targetEntity="TUsuarios", inversedBy="plancha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var \TUsuarios
     *
     * @ORM\ManyToOne(targetEntity="TUsuarios", inversedBy="plancha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="principal_id", referencedColumnName="id")
     * })
     */
    private $principal;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(?string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getPlancha(): ?TPlanchas
    {
        return $this->plancha;
    }

    public function setPlancha(?TPlanchas $plancha): self
    {
        $this->plancha = $plancha;

        return $this;
    }

    public function getUsuario(): ?TUsuarios
    {
        return $this->usuario;
    }

    public function setUsuario(?TUsuarios $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getPrincipal(): ?TUsuarios
    {
        return $this->principal;
    }

    public function setPrincipal(?TUsuarios $principal): self
    {
        $this->principal = $principal;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'plancha' => $this->plancha,
            'usuario' => $this->usuario,
            'tipo' => $this->tipo,
            'principal' => $this->principal
        ];
    }


}
