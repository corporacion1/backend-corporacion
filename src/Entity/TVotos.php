<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TVotos
 *
 * @ORM\Table(name="t_votos", indexes={@ORM\Index(name="fki_t_votos_usuario_id_fkey", columns={"usuario_id"}), @ORM\Index(name="fki_t_votos_plancha_id_fkey", columns={"plancha_id"})})
 * @ORM\Entity
 */
class TVotos implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="t_votos_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fecha_voto", type="string", nullable=true)
     */
    private $fechaVoto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip_computo", type="string", nullable=true)
     */
    private $ipComputo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sistema_operativo", type="string", nullable=true)
     */
    private $so;

    /**
     * @var string|null
     *
     * @ORM\Column(name="navegador", type="string", nullable=true)
     */
    private $navegador;

    /**
     * @var \TUsuarios
     *
     * @ORM\ManyToOne(targetEntity="TUsuarios", inversedBy="votos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="apoderado_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var \TPlanchas
     *
     * @ORM\ManyToOne(targetEntity="TPlanchas", inversedBy="votos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plancha_id", referencedColumnName="id")
     * })
     */
    private $plancha;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaVoto(): ?string
    {
        return $this->fechaVoto;
    }

    public function setFechaVoto(?string $fechaVoto): self
    {
        $this->fechaVoto = $fechaVoto;

        return $this;
    }

    public function getIpComputo(): ?string
    {
        return $this->ipComputo;
    }

    public function setIpComputo(?string $ipComputo): self
    {
        $this->ipComputo = $ipComputo;

        return $this;
    }

    public function getSO(): ?string
    {
        return $this->so;
    }

    public function setSO(?string $so): self
    {
        $this->so = $so;

        return $this;
    }

    public function getNavegador(): ?string
    {
        return $this->navegador;
    }

    public function setNavegador(?string $navegador): self
    {
        $this->navegador = $navegador;

        return $this;
    }

    public function getUsuario(): ?TUsuarios
    {
        return $this->usuario;
    }

    public function setUsuario(?TUsuarios $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getPlancha(): ?TPlanchas
    {
        return $this->plancha;
    }

    public function setPlancha(?TPlanchas $plancha): self
    {
        $this->plancha = $plancha;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'fechaVoto' => $this->fechaVoto,
            'ip' => $this->ipComputo,
            'so' => $this->so,
            'navegador' => $this->navegador,
            'apoderado' => $this->usuario,
            'plancha' => $this->plancha
        ];

    }

}

