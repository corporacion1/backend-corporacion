<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TUsuarios
 *
 * @ORM\Table(name="t_usuarios", indexes={@ORM\Index(name="fki_t_usuarios_tipo_documento_id_fkey", columns={"tipo_documento_id"}), @ORM\Index(name="fki_t_usuarios_rol_id_fkey", columns={"rol_id"})})
 * @ORM\Entity
 */
class TUsuarios implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="t_usuarios_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombres", type="string", nullable=true)
     */
    private $nombres;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellidos", type="string", nullable=true)
     */
    private $apellidos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="documento", type="string", nullable=true)
     */
    private $documento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="correo", type="string", nullable=true)
     */
    private $correo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario", type="string", nullable=true)
     */
    private $usuario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contraseina", type="string", nullable=true)
     */
    private $contraseina;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fecha_exp", type="string", nullable=true)
     */
    private $fechaExp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="num_acciones", type="string", nullable=true)
     */
    private $acciones;

    /**
     * @var \TRoles
     *
     * @ORM\ManyToOne(targetEntity="TRoles", inversedBy="usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rol_id", referencedColumnName="id")
     * })
     */
    private $rol;

    /**
     * @var \TGeneral
     *
     * @ORM\ManyToOne(targetEntity="TGeneral", inversedBy="usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_documento_id", referencedColumnName="id")
     * })
     */
    private $tipoDocumento;

    /**
     * @var \TGeneral
     *
     * @ORM\ManyToOne(targetEntity="TGeneral", inversedBy="usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado_id", referencedColumnName="id")
     * })
     */
    private $estado;

    //Relación con otras entidades
    //============================

    /*
    * @ORM\OneToMany(targetEntity="App\Entity\TVotos", mappedBy="usuario")
    * @ORM\OneToMany(targetEntity="App\Entity\TSesiones", mappedBy="usuario")
    * @ORM\OneToMany(targetEntity="App\Entity\TApoderado", mappedBy="apoderado")
    * @ORM\OneToMany(targetEntity="App\Entity\TVotosConsulta", mappedBy="usuario")
     * @ORM\OneToMany(targetEntity="App\Entity\TApoderado", mappedBy="accionista")
     * @ORM\OneToMany(targetEntity="App\Entity\TUsuariosPlancha", mappedBy="usuario")
     * @ORM\OneToMany(targetEntity="App\Entity\TUsuariosPlancha", mappedBy="principal")
    */

    private $votos;
    private $sesion;
    private $plancha;
    private $apoderado;
    private $votoConsulta;

    public function __construct(){
       $this->votos = new ArrayCollection();
       $this->sesion = new ArrayCollection();
       $this->plancha = new ArrayCollection();
       $this->apoderado = new ArrayCollection();
       $this->votoConsulta = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombres(): ?string
    {
        return $this->nombres;
    }

    public function setNombres(?string $nombres): self
    {
        $this->nombres = $nombres;

        return $this;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(?string $apellidos): self
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getDocumento(): ?string
    {
        return $this->documento;
    }

    public function setDocumento(?string $documento): self
    {
        $this->documento = $documento;

        return $this;
    }

    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    public function setCorreo(?string $correo): self
    {
        $this->correo = $correo;

        return $this;
    }

    public function getUsuario(): ?string
    {
        return $this->usuario;
    }

    public function setUsuario(?string $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getContraseina(): ?string
    {
        return $this->contraseina;
    }

    public function setContraseina(?string $contraseina): self
    {
        $this->contraseina = $contraseina;

        return $this;
    }

    public function getFechaExp(): ?string
    {
        return $this->fechaExp;
    }

    public function setFechaExp(?string $fechaExp): self
    {
        $this->fechaExp = $fechaExp;

        return $this;
    }

    public function getAcciones(): ?string
    {
        return $this->acciones;
    }

    public function setAcciones(?string $acciones): self
    {
        $this->acciones = $acciones;

        return $this;
    }

    public function getRol(): ?TRoles
    {
        return $this->rol;
    }

    public function setRol(?TRoles $rol): self
    {
        $this->rol = $rol;

        return $this;
    }

    public function getTipoDocumento(): ?TGeneral
    {
        return $this->tipoDocumento;
    }

    public function setTipoDocumento(?TGeneral $tipoDocumento): self
    {
        $this->tipoDocumento = $tipoDocumento;

        return $this;
    }

    public function getEstado(): ?TGeneral
    {
        return $this->estado;
    }

    public function setEstado(?TGeneral $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'nombres' => $this->nombres,
            'apellidos' => $this->apellidos,
            'tipoDocumento' => $this->tipoDocumento,
            'documento' => $this->documento,
            'correo' => $this->correo,
            'usuario' => $this->usuario,
            'fechaExp' => $this->fechaExp,
            'rol' => $this->rol,
            'estado' => $this->estado,
            'acciones' => $this->acciones
        ];

    }

}
