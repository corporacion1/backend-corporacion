<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TGeneral
 *
 * @ORM\Table(name="t_general")
 * @ORM\Entity
 */
class TGeneral implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="t_general_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="string", nullable=true)
     */
    private $valor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", nullable=true)
     */
    private $categoria;

    //Relación con otras entidades
    //============================

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TUsuarios", mappedBy="estado")
     * @ORM\OneToMany(targetEntity="App\Entity\TRegistro", mappedBy="estado")
     * @ORM\OneToMany(targetEntity="App\Entity\TPlanchas", mappedBy="estado")
     * @ORM\OneToMany(targetEntity="App\Entity\TUsuarios", mappedBy="tipoDocumento")
     * @ORM\OneToMany(targetEntity="App\Entity\TRegistro", mappedBy="tipoDocumento")
     * @ORM\OneToMany(targetEntity="App\Entity\TRespuestasConsulta", mappedBy="estado")
    */

    private $usuario;
    private $plancha;
    private $registro;
    private $respuesta;

    public function __construct(){
        $this->usuario = new ArrayCollection();
        $this->plancha = new ArrayCollection();
       $this->registro = new ArrayCollection();
       $this->respuesta = new ArrayCollection();
   }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getValor(): ?string
    {
        return $this->valor;
    }

    public function setValor(?string $valor): self
    {
        $this->valor = $valor;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getCategoria(): ?string
    {
        return $this->categoria;
    }

    public function setCategoria(?string $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'valor' => $this->valor,
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'categoria' => $this->categoria
        ];

    }

}
